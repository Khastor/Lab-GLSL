#version 440

///////////////////////////////////////////////////////////////////////////////////////////////////
// Library > Functions                                                                           //
///////////////////////////////////////////////////////////////////////////////////////////////////

#define EPSILON 0.00001

/**
 * @brief Exponential Easing.
 *
 * In this implementation of an exponential shaping function, the control parameter enables varying the function from an ease-out to an ease-in form.
 *
 * @param pX The input value, in the range [0, 1].
 * @param pA The exponential control parameter, in the range (0, 1).
 * @return The output value, in the range [0, 1].
 * 
 * @author Golan Levin http://www.flong.com/archive/texts/code/shapers_exp/.
 */
float ExponentialEasing(in float pX, in float pA)
{
    if (pA < 0.5)
    {
        // Map the emphasis pA to the range (0, 1].
        pA = clamp(2.0 * pA, EPSILON, 1.0);
        return pow(pX, pA);
    }
    else
    {
        // Map the de-emphasis pA to the range [0, 1).
        pA = clamp(2.0 * (pA - 0.5), 0.0, 1.0 - EPSILON);
        return pow(pX, 1.0 / (1.0 - pA));
    }
}

///////////////////////////////////////////////////////////////////////////////////////////////////
// Library > Functions Demos                                                                     //
///////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * @brief The exponential control parameter, in the range (0, 1).
 *
 * @label Emphasis
 * @default 0.5
 * @minimum 0.01
 * @maximum 0.99
 */
uniform float uA;

/**
 * @brief The viewport resolution, in pixels.
 */
uniform vec2 uFrameResolution;

/**
 * @brief The fragment shader output color.
 */
out vec4 FragmentColor;

void main()
{
    // Clamp the current point to the unit square.
    vec2 lSquare = vec2(min(uFrameResolution.x, uFrameResolution.y));
    vec2 lPoint = min(gl_FragCoord.xy, lSquare) / lSquare;

    FragmentColor = vec4(vec3(step(ExponentialEasing(lPoint.x, uA), lPoint.y)), 1.0);
}
