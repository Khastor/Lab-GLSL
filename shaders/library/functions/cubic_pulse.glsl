#version 440

///////////////////////////////////////////////////////////////////////////////////////////////////
// Library > Functions                                                                           //
///////////////////////////////////////////////////////////////////////////////////////////////////

#define EPSILON 0.00001

/**
 * @brief Cubic Pulse.
 *
 * The cubic pulse can be a performant replacement for a gaussian.
 * Use it to replace symmetric smoothsteps to isolate some features in a signal.
 *
 * @param pX The input value, in the range (-INF, +INF).
 * @param pC The center of the cubic pulse, in the range (-INF, +INF).
 * @param pW The width of the cubic pulse, in the range (0, +INF).
 * @return The output value, in the range [0, 1].
 *
 * @author Inigo Quilez https://iquilezles.org/articles/functions/.
 */
float CubicPulse(in float pX, in float pC, in float pW)
{
    // Enforce pre-conditions on parameter pW.
    // This is to avoid divisions by zero.
    pW = max(pW, EPSILON);

    pX = abs(pX - pC);
    if (pX >= pW)
    {
        return 0.0;
    }
    pX = pX / pW;
    return 1.0 - pX * pX * (3.0 - 2.0 * pX);
}

///////////////////////////////////////////////////////////////////////////////////////////////////
// Library > Functions Demos                                                                     //
///////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * @brief The center of the cubic pulse, in the range (-INF, +INF).
 *
 * @label Center
 * @default 0.5
 * @minimum 0.0
 * @maximum 1.0
 */
uniform float uC;

/**
 * @brief The width of the cubic pulse, in the range (0, +INF).
 *
 * @label Width
 * @default 0.5
 * @minimum 0.01
 * @maximum 100.0
 */
uniform float uW;

/**
 * @brief The viewport resolution, in pixels.
 */
uniform vec2 uFrameResolution;

/**
 * @brief The fragment shader output color.
 */
out vec4 FragmentColor;

void main()
{
    // Clamp the current point to the unit square.
    vec2 lSquare = vec2(min(uFrameResolution.x, uFrameResolution.y));
    vec2 lPoint = min(gl_FragCoord.xy, lSquare) / lSquare;

    FragmentColor = vec4(vec3(step(CubicPulse(lPoint.x, uC, uW), lPoint.y)), 1.0);
}
