#version 440

///////////////////////////////////////////////////////////////////////////////////////////////////
// Library > Functions                                                                           //
///////////////////////////////////////////////////////////////////////////////////////////////////

#define EPSILON 0.00001

/**
 * @brief Smoothstep Integral.
 *
 * @param pX The input value, in the range (-INF, +INF).
 * @param pT The threshold after which the smoothing ends, in the range (0, +INF).
 * @return The output value.
 *
 * @author Inigo Quilez https://iquilezles.org/articles/functions/.
 */
float SmoothstepIntegral(in float pX, in float pT)
{
    // Enforce pre-conditions on parameter pT.
    // This is to avoid divisions by zero.
    pT = max(pT, EPSILON);

    if (pX > pT)
    {
        return pX - pT / 2.0;
    }
    return pX * pX * pX * (1.0 - 0.5 * pX / pT) / pT / pT;
}

///////////////////////////////////////////////////////////////////////////////////////////////////
// Library > Functions Demos                                                                     //
///////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * @brief The threshold after which the smoothing ends, in the range (0, +INF).
 *
 * @label Threshold
 * @default 1.0
 * @minimum 0.01
 * @maximum 0.99
 */
uniform float uT;

/**
 * @brief The viewport resolution, in pixels.
 */
uniform vec2 uFrameResolution;

/**
 * @brief The fragment shader output color.
 */
out vec4 FragmentColor;

void main()
{
    // Clamp the current point to the unit square.
    vec2 lSquare = vec2(min(uFrameResolution.x, uFrameResolution.y));
    vec2 lPoint = min(gl_FragCoord.xy, lSquare) / lSquare;

    FragmentColor = vec4(vec3(step(SmoothstepIntegral(lPoint.x, uT), lPoint.y)), 1.0);
}
