#version 440

///////////////////////////////////////////////////////////////////////////////////////////////////
// Library > Functions                                                                           //
///////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * @brief Smooth Clip Identity.
 *
 * Smoothly clip the Identity function to a lower bound value.
 * This function works best for small lower bound values, as it introduces a bias to the Identity function.
 *
 * @param pX The input value, in the range [0, +INF).
 * @param pL The lower bound value, in the range [0, +INF).
 * @return The output value, in the range [pL, +INF).
 */
float SmoothClipIndentity(in float pX, in float pL)
{
    return sqrt(pX * pX + pL * pL);
}

///////////////////////////////////////////////////////////////////////////////////////////////////
// Library > Functions Demos                                                                     //
///////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * @brief The lower bound value, in the range [0, +INF).
 *
 * @label Lower Bound
 * @default 0.2
 * @minimum 0.0
 * @maximum 1.0
 */
uniform float uL;

/**
 * @brief The viewport resolution, in pixels.
 */
uniform vec2 uFrameResolution;

/**
 * @brief The fragment shader output color.
 */
out vec4 FragmentColor;

void main()
{
    // Clamp the current point to the unit square.
    vec2 lSquare = vec2(min(uFrameResolution.x, uFrameResolution.y));
    vec2 lPoint = min(gl_FragCoord.xy, lSquare) / lSquare;

    FragmentColor = vec4(vec3(step(SmoothClipIndentity(lPoint.x, uL), lPoint.y)), 1.0);
}
