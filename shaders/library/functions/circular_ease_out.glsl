#version 440

///////////////////////////////////////////////////////////////////////////////////////////////////
// Library > Functions                                                                           //
///////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * @brief Circular Ease-Out.
 *
 * A circular arc for easing out of the unit square.
 * The computational efficiency of this method is diminished by its use of a square root.
 *
 * @param pX The input value, in the range [0, 1].
 * @return The output value, in the range [0, 1].
 * 
 * @author Golan Levin http://www.flong.com/archive/texts/code/shapers_circ/.
 */
float CircularEaseOut(in float pX)
{
    return sqrt(1.0 - (1.0 - pX) * (1.0 - pX));
}

///////////////////////////////////////////////////////////////////////////////////////////////////
// Library > Functions Demos                                                                     //
///////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * @brief The viewport resolution, in pixels.
 */
uniform vec2 uFrameResolution;

/**
 * @brief The fragment shader output color.
 */
out vec4 FragmentColor;

void main()
{
    // Clamp the current point to the unit square.
    vec2 lSquare = vec2(min(uFrameResolution.x, uFrameResolution.y));
    vec2 lPoint = min(gl_FragCoord.xy, lSquare) / lSquare;

    FragmentColor = vec4(vec3(step(CircularEaseOut(lPoint.x), lPoint.y)), 1.0);
}
