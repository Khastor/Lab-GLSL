#version 440

///////////////////////////////////////////////////////////////////////////////////////////////////
// Library > Functions                                                                           //
///////////////////////////////////////////////////////////////////////////////////////////////////

#define EPSILON 0.00001

/**
 * @brief Double-Exponential Seat.
 *
 * A seat-shaped function can be created with a coupling of two exponential functions.
 *
 * @param pX The input value, in the range [0, 1].
 * @param pA The flatness control parameter, in the range [0, 1).
 * @return The output value, in the range [0, 1].
 * 
 * @author Golan Levin http://www.flong.com/archive/texts/code/shapers_exp/.
 */
float DoubleExponentialSeat(in float pX, in float pA)
{
    // Enforce pre-conditions on parameter pA.
    // This is to avoid undefined results when both the base and exponent are zero.
    pA = clamp(pA, 0.0, 1.0 - EPSILON);

    if (pX <= 0.5)
    {
        return 0.5 * pow(2.0 * pX, 1.0 - pA);
    }
    else
    {
        return 1.0 - 0.5 * pow(2.0 * (1.0 - pX), 1.0 - pA);
    }
}

///////////////////////////////////////////////////////////////////////////////////////////////////
// Library > Functions Demos                                                                     //
///////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * @brief The flatness control parameter, in the range [0, 1).
 *
 * @label Flatness
 * @default 0.5
 * @minimum 0.0
 * @maximum 0.99
 */
uniform float uA;

/**
 * @brief The viewport resolution, in pixels.
 */
uniform vec2 uFrameResolution;

/**
 * @brief The fragment shader output color.
 */
out vec4 FragmentColor;

void main()
{
    // Clamp the current point to the unit square.
    vec2 lSquare = vec2(min(uFrameResolution.x, uFrameResolution.y));
    vec2 lPoint = min(gl_FragCoord.xy, lSquare) / lSquare;

    FragmentColor = vec4(vec3(step(DoubleExponentialSeat(lPoint.x, uA), lPoint.y)), 1.0);
}
