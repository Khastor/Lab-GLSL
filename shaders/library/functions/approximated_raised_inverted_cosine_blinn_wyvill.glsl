#version 440

///////////////////////////////////////////////////////////////////////////////////////////////////
// Library > Functions                                                                           //
///////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * @brief Blinn-Wyvill Approximation to the Raised Inverted Cosine.
 *
 * This is a polynomial approximation to the raised inverted cosine, with less than 0.1% error in the normalized range [0, 1].
 * This approximation has flat derivatives in 0 and 1, and passes through (0.5, 0.5).
 *
 * @param pX The input value, in the normalized range [0, 1] which maps to the range [-PI, 0] of the cosine function.
 * @return The output value, in the normalized range [0, 1].
 * 
 * @author Golan Levin http://www.flong.com/archive/texts/code/shapers_poly/.
 */
float BlinnWyvillRaisedInvertedCosine(in float pX)
{
    float lX2 = pX * pX;
    float lX4 = lX2 * lX2;
    float lX6 = lX4 * lX2;
    return (4.0/9.0) * lX6 - (17.0/9.0) * lX4 + (22.0/9.0) * lX2;
}

///////////////////////////////////////////////////////////////////////////////////////////////////
// Library > Functions Demos                                                                     //
///////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * @brief The viewport resolution, in pixels.
 */
uniform vec2 uFrameResolution;

/**
 * @brief The fragment shader output color.
 */
out vec4 FragmentColor;

void main()
{
    // Clamp the current point to the unit square.
    vec2 lSquare = vec2(min(uFrameResolution.x, uFrameResolution.y));
    vec2 lPoint = min(gl_FragCoord.xy, lSquare) / lSquare;

    FragmentColor = vec4(vec3(step(BlinnWyvillRaisedInvertedCosine(lPoint.x), lPoint.y)), 1.0);
}
