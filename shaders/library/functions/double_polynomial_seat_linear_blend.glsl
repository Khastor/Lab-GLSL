#version 440

///////////////////////////////////////////////////////////////////////////////////////////////////
// Library > Functions                                                                           //
///////////////////////////////////////////////////////////////////////////////////////////////////

#define EPSILON 0.00001

/**
 * @brief Double-Polynomial Seat with Linear Blend.
 *
 * This seat-shaped function is formed by joining two polynomial curves using an odd integer exponent.
 * The curves meet at the inflection point along the diagonal of the unit square.
 * The slope of the curve in the vicinity of the inflection point is controlled by blending the curve with the Identity function.
 *
 * The adjustable flattening around the inflection point makes this function useful for magnifying evenly-spaced data.
 *
 * @param pX The input value, in the range [0, 1].
 * @param pN The control parameter for the breadth of the plateau in the vicinity of the inflection point.
 * @param pI The control parameter for the location of the inflection point along the diagonal of the unit square, in the range (0, 1).
 * @param pB The amount of linear blend with the Identity function, in the range [0, 1].
 * @return The output value, in the normalized range [0, 1].
 * 
 * @author Golan Levin http://www.flong.com/archive/texts/code/shapers_poly/.
 */
float DoublePolynomialSeatLinearBlend(in float pX, in uint pN, in float pI, in float pB)
{
    // Enforce pre-conditions on parameter pI.
    // This is to avoid divisions by zero.
    pI = clamp(pI, EPSILON, 1.0 - EPSILON);

    float lExponent = float(2 * pN + 1);
    if (pX <= pI)
    {
        return pB * pX + (1.0 - pB) * (pI - pI * pow(1.0 - pX / pI, lExponent));
    }
    else
    {
        return pB * pX + (1.0 - pB) * (pI + (1.0 - pI) * pow((pX - pI) / (1.0 - pI), lExponent));
    }
}

///////////////////////////////////////////////////////////////////////////////////////////////////
// Library > Functions Demos                                                                     //
///////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * @brief The control parameter for the breadth of the plateau in the vicinity of the inflection point.
 *
 * @label Breadth
 * @default 1
 * @minimum 0
 * @maximum 10
 */
uniform uint uN;

/**
 * @brief The control parameter for the location of the inflection point along the diagonal of the unit square, in the range (0, 1).
 *
 * @label Inflection Point
 * @default 0.5
 * @minimum 0.01
 * @maximum 0.99
 */
uniform float uI;

/**
 * @brief The amount of linear blend with the Identity function, in the range [0, 1].
 *
 * @label Linear Blend
 * @default 0.5
 * @minimum 0.0
 * @maximum 1.0
 */
uniform float uB;

/**
 * @brief The viewport resolution, in pixels.
 */
uniform vec2 uFrameResolution;

/**
 * @brief The fragment shader output color.
 */
out vec4 FragmentColor;

void main()
{
    // Clamp the current point to the unit square.
    vec2 lSquare = vec2(min(uFrameResolution.x, uFrameResolution.y));
    vec2 lPoint = min(gl_FragCoord.xy, lSquare) / lSquare;

    FragmentColor = vec4(vec3(step(DoublePolynomialSeatLinearBlend(lPoint.x, uN, uI, uB), lPoint.y)), 1.0);
}
