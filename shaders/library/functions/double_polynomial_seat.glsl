#version 440

///////////////////////////////////////////////////////////////////////////////////////////////////
// Library > Functions                                                                           //
///////////////////////////////////////////////////////////////////////////////////////////////////

#define EPSILON 0.00001

/**
 * @brief Double-Polynomial Seat.
 *
 * This seat-shaped function is formed by joining two polynomial curves using an odd integer exponent.
 * The curves meet horizontally at the inflection point pI in the unit square.
 *
 * @param pX The input value, in the range [0, 1].
 * @param pN The control parameter for the breadth of the plateau in the vicinity of the inflection point.
 * @param pI The inflection point, in the unit square.
 * @return The output value, in the normalized range [0, 1].
 * 
 * @author Golan Levin http://www.flong.com/archive/texts/code/shapers_poly/.
 */
float DoublePolynomialSeat(in float pX, in uint pN, in vec2 pI)
{
    // Enforce pre-conditions on parameter pI.
    // This is to avoid divisions by zero.
    pI.x = clamp(pI.x, EPSILON, 1.0 - EPSILON);

    float lExponent = float(2 * pN + 1);
    if (pX <= pI.x)
    {
        return pI.y - pI.y * pow(1.0 - pX / pI.x, lExponent);
    }
    else
    {
        return pI.y + (1.0 - pI.y) * pow((pX - pI.x) / (1.0 - pI.x), lExponent);
    }
}

///////////////////////////////////////////////////////////////////////////////////////////////////
// Library > Functions Demos                                                                     //
///////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * @brief The control parameter for the breadth of the plateau in the vicinity of the inflection point.
 *
 * @label Breadth
 * @default 1
 * @minimum 0
 * @maximum 10
 */
uniform uint uN;

/**
 * @brief The inflection point, in the unit square.
 *
 * @label Inflection Point
 * @default 0.5, 0.5
 * @minimum 0.0
 * @maximum 1.0
 */
uniform vec2 uI;

/**
 * @brief The viewport resolution, in pixels.
 */
uniform vec2 uFrameResolution;

/**
 * @brief The fragment shader output color.
 */
out vec4 FragmentColor;

void main()
{
    // Clamp the current point to the unit square.
    vec2 lSquare = vec2(min(uFrameResolution.x, uFrameResolution.y));
    vec2 lPoint = min(gl_FragCoord.xy, lSquare) / lSquare;

    FragmentColor = vec4(vec3(step(DoublePolynomialSeat(lPoint.x, uN, uI), lPoint.y)), 1.0);
}
