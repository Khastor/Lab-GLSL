#version 440

///////////////////////////////////////////////////////////////////////////////////////////////////
// Library > Functions                                                                           //
///////////////////////////////////////////////////////////////////////////////////////////////////

#define EPSILON 0.00001

/**
 * @brief Exponential Ease-In.
 *
 * @param pX The input value, in the range [0, 1].
 * @param pA The emphasis, in the range (0, 1].
 * @return The output value, in the range [0, 1].
 * 
 * @author Golan Levin http://www.flong.com/archive/texts/code/shapers_exp/.
 */
float ExponentialEaseIn(in float pX, in float pA)
{
    // Enforce pre-conditions on parameter pA.
    // This is to avoid divisions by zero.
    pA = clamp(pA, EPSILON, 1.0);

    return pow(pX, 1.0 / pA);
}

///////////////////////////////////////////////////////////////////////////////////////////////////
// Library > Functions Demos                                                                     //
///////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * @brief The emphasis, in the range (0, 1].
 *
 * @label Emphasis
 * @default 0.5
 * @minimum 0.01
 * @maximum 1.0
 */
uniform float uA;

/**
 * @brief The viewport resolution, in pixels.
 */
uniform vec2 uFrameResolution;

/**
 * @brief The fragment shader output color.
 */
out vec4 FragmentColor;

void main()
{
    // Clamp the current point to the unit square.
    vec2 lSquare = vec2(min(uFrameResolution.x, uFrameResolution.y));
    vec2 lPoint = min(gl_FragCoord.xy, lSquare) / lSquare;

    FragmentColor = vec4(vec3(step(ExponentialEaseIn(lPoint.x, uA), lPoint.y)), 1.0);
}
