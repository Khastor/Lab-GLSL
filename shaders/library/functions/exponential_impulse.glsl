#version 440

///////////////////////////////////////////////////////////////////////////////////////////////////
// Library > Functions                                                                           //
///////////////////////////////////////////////////////////////////////////////////////////////////

#define EPSILON 0.00001

/**
 * @brief Exponential Impulse.
 *
 * Impulses are great for triggering behaviours or making envelopes for music or animation.
 * The maximum output value is 1 for x = 1 / k where k is the stretching.
 *
 * @param pX The input value, in the range [0, +INF).
 * @param pK The control parameter for the stretching, in the range [1, +INF).
 * @return The output value, in the range [0, 1].
 *
 * @author Inigo Quilez https://iquilezles.org/articles/functions/.
 */
float ExponentialImpulse(in float pX, in float pK)
{
    float pH = pK * pX;
    return pH * exp(1.0 - pH);
}

///////////////////////////////////////////////////////////////////////////////////////////////////
// Library > Functions Demos                                                                     //
///////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * @brief The control parameter for the stretching, in the range [1, +INF).
 *
 * @label Stretching
 * @default 4.0
 * @minimum 1.0
 * @maximum 100.0
 */
uniform float uK;

/**
 * @brief The viewport resolution, in pixels.
 */
uniform vec2 uFrameResolution;

/**
 * @brief The fragment shader output color.
 */
out vec4 FragmentColor;

void main()
{
    // Clamp the current point to the unit square.
    vec2 lSquare = vec2(min(uFrameResolution.x, uFrameResolution.y));
    vec2 lPoint = min(gl_FragCoord.xy, lSquare) / lSquare;

    FragmentColor = vec4(vec3(step(ExponentialImpulse(lPoint.x, uK), lPoint.y)), 1.0);
}
