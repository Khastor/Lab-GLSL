#version 440

///////////////////////////////////////////////////////////////////////////////////////////////////
// Library > Functions                                                                           //
///////////////////////////////////////////////////////////////////////////////////////////////////

#define EPSILON 0.00001

/**
 * @brief Parabola.
 *
 * Remap the [0, 1] interval into [0, 1] such that the corners are mapped to 0 and the center to 1.
 *
 * @param pX The input value, in the range [0, 1].
 * @param pK The shape control parameter, in the range (0, +INF].
 * @return The output value, in the range [0, 1].
 *
 * @author Inigo Quilez https://iquilezles.org/articles/functions/.
 */
float Parabola(in float pX, in float pK)
{
    // Enforce pre-conditions on parameter pK.
    // This is to avoid undefined results when both the base and exponent are zero.
    pK = max(pK, EPSILON);

    return pow(4.0 * pX * (1.0 - pX), pK);
}

///////////////////////////////////////////////////////////////////////////////////////////////////
// Library > Functions Demos                                                                     //
///////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * @brief The shape control parameter, in the range (0, +INF].
 *
 * @label Shape
 * @default 1.0
 * @minimum 0.01
 * @maximum 100.0
 */
uniform float uK;

/**
 * @brief The viewport resolution, in pixels.
 */
uniform vec2 uFrameResolution;

/**
 * @brief The fragment shader output color.
 */
out vec4 FragmentColor;

void main()
{
    // Clamp the current point to the unit square.
    vec2 lSquare = vec2(min(uFrameResolution.x, uFrameResolution.y));
    vec2 lPoint = min(gl_FragCoord.xy, lSquare) / lSquare;

    FragmentColor = vec4(vec3(step(Parabola(lPoint.x, uK), lPoint.y)), 1.0);
}
