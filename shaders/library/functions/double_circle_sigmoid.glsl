#version 440

///////////////////////////////////////////////////////////////////////////////////////////////////
// Library > Functions                                                                           //
///////////////////////////////////////////////////////////////////////////////////////////////////

#define SQ(x) ((x) * (x))

/**
 * @brief Double-Circle Sigmoid.
 *
 * This sigmoidal shaping function is formed by the meeting of two circular arcs, which join with a vertical tangent.
 *
 * @param pX The input value, in the range [0, 1].
 * @param pA The location of the inflection point along the diagonal of the unit square, in the range [0, 1].
 * @return The output value, in the range [0, 1].
 * 
 * @author Golan Levin http://www.flong.com/archive/texts/code/shapers_circ/.
 */
float DoubleCircleSigmoid(in float pX, in float pA)
{
    if (pX <= pA)
    {
        return pA - sqrt(SQ(pA) - SQ(pX));
    }
    else
    {
        return pA + sqrt(SQ(1.0 - pA) - SQ(pX - 1.0));
    }
}

///////////////////////////////////////////////////////////////////////////////////////////////////
// Library > Functions Demos                                                                     //
///////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * @brief The location of the inflection point along the diagonal of the unit square, in the range [0, 1].
 *
 * @label Inflection Point
 * @default 0.5
 * @minimum 0.0
 * @maximum 1.0
 */
uniform float uA;

/**
 * @brief The viewport resolution, in pixels.
 */
uniform vec2 uFrameResolution;

/**
 * @brief The fragment shader output color.
 */
out vec4 FragmentColor;

void main()
{
    // Clamp the current point to the unit square.
    vec2 lSquare = vec2(min(uFrameResolution.x, uFrameResolution.y));
    vec2 lPoint = min(gl_FragCoord.xy, lSquare) / lSquare;

    FragmentColor = vec4(vec3(step(DoubleCircleSigmoid(lPoint.x, uA), lPoint.y)), 1.0);
}
