#version 440

///////////////////////////////////////////////////////////////////////////////////////////////////
// Library > Functions                                                                           //
///////////////////////////////////////////////////////////////////////////////////////////////////

#define EPSILON 0.00001

/**
 * @brief Falloff.
 *
 * Quadratic falloff, reaching zero at a the given distance.
 *
 * @param pX The input value, in the range [0, +INF).
 * @param pF The falloff distance, in the range (1, +INF].
 * @return The output value, in the range [0, 1].
 *
 * @author Inigo Quilez https://iquilezles.org/articles/functions/.
 */
float Falloff(in float pX, in float pF)
{
    // Enforce pre-conditions on parameter pF.
    // This is to avoid divisions by zero.
    pF = max(pF, 1.0 + EPSILON);

    pX = 1.0 / ((pX + 1.0) * (pX + 1.0));
    pF = 1.0 / ((pF + 1.0) * (pF + 1.0));
    return (pX - pF) / (1.0 - pF);
}

///////////////////////////////////////////////////////////////////////////////////////////////////
// Library > Functions Demos                                                                     //
///////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * @brief The falloff distance, in the range (1, +INF].
 *
 * @label Falloff Distance
 * @default 2.0
 * @minimum 1.01
 * @maximum 100.0
 */
uniform float uF;

/**
 * @brief The viewport resolution, in pixels.
 */
uniform vec2 uFrameResolution;

/**
 * @brief The fragment shader output color.
 */
out vec4 FragmentColor;

void main()
{
    // Clamp the current point to the unit square.
    vec2 lSquare = vec2(min(uFrameResolution.x, uFrameResolution.y));
    vec2 lPoint = min(gl_FragCoord.xy, lSquare) / lSquare;

    FragmentColor = vec4(vec3(step(Falloff(lPoint.x, uF), lPoint.y)), 1.0);
}
