#version 440

///////////////////////////////////////////////////////////////////////////////////////////////////
// Library > Functions                                                                           //
///////////////////////////////////////////////////////////////////////////////////////////////////

#define EPSILON 0.00001

/**
 * @brief Double-Polynomial Sigmoid (Odd).
 *
 * This sigmoid-shaped function is formed by joining two symmetric polynomials using odd exponents at the center of the unit square.
 * The sigmoid has flat derivatives in 0 and 1, and passes through (0.5, 0.5).
 *
 * @param pX The input value, in the range [0, 1].
 * @param pN The control parameter for the steepness of the sigmoid.
 * @return The output value, in the normalized range [0, 1].
 * 
 * @author Golan Levin http://www.flong.com/archive/texts/code/shapers_poly/.
 */
float DoublePolynomialSigmoidOdd(in float pX, in uint pN)
{
    float lExponent = float(2 * pN + 1);
    if (pX <= 0.5)
    {
        return pow(2.0 * pX, lExponent) / 2.0;
    }
    else
    {
        return 1.0 + pow(2.0 * pX - 2.0, lExponent) / 2.0;
    }
}

///////////////////////////////////////////////////////////////////////////////////////////////////
// Library > Functions Demos                                                                     //
///////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * @brief The control parameter for the steepness of the sigmoid.
 *
 * @label Steepness
 * @default 1
 * @minimum 0
 * @maximum 10
 */
uniform uint uN;

/**
 * @brief The viewport resolution, in pixels.
 */
uniform vec2 uFrameResolution;

/**
 * @brief The fragment shader output color.
 */
out vec4 FragmentColor;

void main()
{
    // Clamp the current point to the unit square.
    vec2 lSquare = vec2(min(uFrameResolution.x, uFrameResolution.y));
    vec2 lPoint = min(gl_FragCoord.xy, lSquare) / lSquare;

    FragmentColor = vec4(vec3(step(DoublePolynomialSigmoidOdd(lPoint.x, uN), lPoint.y)), 1.0);
}
