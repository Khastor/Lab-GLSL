#version 440

///////////////////////////////////////////////////////////////////////////////////////////////////
// Library > Functions                                                                           //
///////////////////////////////////////////////////////////////////////////////////////////////////

#define EPSILON 0.00001

/**
 * @brief Parabola.
 *
 * Remap the [0, 1] interval into [0, 1] such that the corners are mapped to 0 and the center to 1.
 *
 * @param pX The input value, in the range [0, 1].
 * @param pK The shape control parameter, in the range (0, +INF].
 * @return The output value, in the range [0, 1].
 *
 * @author Inigo Quilez https://iquilezles.org/articles/functions/.
 */
float Parabola(in float pX, in float pK)
{
    // Enforce pre-conditions on parameter pK.
    // This is to avoid undefined results when both the base and exponent are zero.
    pK = max(pK, EPSILON);

    return pow(4.0 * pX * (1.0 - pX), pK);
}

/**
 * @brief Exponential Impulse.
 *
 * Impulses are great for triggering behaviours or making envelopes for music or animation.
 * The maximum output value is 1 for x = 1 / k where k is the stretching.
 *
 * @param pX The input value, in the range [0, +INF).
 * @param pK The control parameter for the stretching, in the range [1, +INF).
 * @return The output value, in the range [0, 1].
 *
 * @author Inigo Quilez https://iquilezles.org/articles/functions/.
 */
float ExponentialImpulse(in float pX, in float pK)
{
    float pH = pK * pX;
    return pH * exp(1.0 - pH);
}

///////////////////////////////////////////////////////////////////////////////////////////////////
// Practice                                                                                      //
///////////////////////////////////////////////////////////////////////////////////////////////////

#define PI 3.14159265359

/**
 * @brief The frame time, in seconds.
 */
uniform float uFrameTime;

/**
 * @brief The viewport resolution, in pixels.
 */
uniform vec2 uFrameResolution;

/**
 * @brief The fragment shader output color.
 */
out vec4 FragmentColor;

// ---------------------------------------------------------------------------- Signal properties

/**
 * @brief Sample the signal intensity.
 *
 * The signal intensity controls the color of the signal.
 *
 * @param pSamplingCoord The sampling point for each signal component, in the range [-1, 1].
 * @return The sampled intensity value for each signal component, in the range [0, 1].
 */
vec3 SampleSignalIntensity(in vec3 pSamplingCoord)
{
    return vec3(1.0);
}

/**
 * @brief Sample the signal amplitude.
 *
 * The signal amplitude is a mask that controls the shape of the signal.
 *
 * @param pSamplingCoord The sampling point for each signal component, in the range [-1, 1].
 * @return The sampled amplitude value for each signal component, in the range [0, 1].
 */
vec3 SampleSignalAmplitude(in vec3 pSamplingCoord)
{
    return vec3((0.1 + Parabola(0.5 + 0.5 * pSamplingCoord.x, 8.0)) * sin(4.0 * uFrameTime - abs(pSamplingCoord.x) * PI));
}

/**
 * @brief Sample the smoothing of the signal amplitude.
 *
 * The smoothing of the signal amplitude controls how much the signal amplitude mask is smoothed out.
 *
 * @param pSamplingCoord The sampling point for each signal component, in the range [-1, 1].
 * @return The sampled amplitude smoothing value for each signal component, in the range [0, +INF).
 */
vec3 SampleSignalAmplitudeSmoothing(in vec3 pSamplingCoord)
{
    return vec3(
        0.6 * abs(max(0.0, pSamplingCoord.x)),
        0.1,
        0.6 * abs(max(0.0, -pSamplingCoord.x)));
}

// ---------------------------------------------------------------------------- Screen properties

vec3 ComputeHCellFreq(in vec2 pNormalizedCoord)
{
    return vec3(
        24.0 - 12.0 * ExponentialImpulse(mod(uFrameTime, 5.0), 8.0),
        24.0,
        24.0 - 12.0 * ExponentialImpulse(mod(uFrameTime, 7.0), 6.0));
}

vec3 ComputeVCellFreq(in vec2 pNormalizedCoord)
{
    return vec3(
        48.0,
        48.0 - 24.0 * ExponentialImpulse(mod(uFrameTime, 9.0), 2.0),
        48.0);
}

vec3 ComputeHCellSmoothing(in vec2 pNormalizedCoord)
{
    return vec3(
        0.4 * abs(max(0.0, -pNormalizedCoord.x)),
        0.0,
        0.6 * abs(max(0.0, pNormalizedCoord.x)));
}

vec3 ComputeVCellSmoothing(in vec2 pNormalizedCoord)
{
    return vec3(0.2, 0.0, 0.4);
}

// ---------------------------------------------------------------------------- Screen lighting

vec3 ComputeCellIntensity(in vec2 pNormalizedCoord)
{
    // Compute the cell pattern properties for each signal component.
    vec3 lHCellFreq = ComputeHCellFreq(pNormalizedCoord);
    vec3 lVCellFreq = ComputeVCellFreq(pNormalizedCoord);

    // Compute the cell pattern mask for each signal component.
    vec3 lHCellSmoothing = ComputeHCellSmoothing(pNormalizedCoord);
    vec3 lVCellSmoothing = ComputeVCellSmoothing(pNormalizedCoord);
    vec3 lHCellMask = vec3(
        smoothstep(-lHCellSmoothing.r, 0.3, abs(cos(PI * lHCellFreq.r * pNormalizedCoord.x))),
        smoothstep(-lHCellSmoothing.g, 0.3, abs(cos(PI * lHCellFreq.g * pNormalizedCoord.x))),
        smoothstep(-lHCellSmoothing.b, 0.3, abs(cos(PI * lHCellFreq.b * pNormalizedCoord.x))));
    vec3 lVCellMask = vec3(
        smoothstep(-lVCellSmoothing.r, 0.6, abs(cos(PI * lVCellFreq.r * pNormalizedCoord.y))),
        smoothstep(-lVCellSmoothing.g, 0.6, abs(cos(PI * lVCellFreq.g * pNormalizedCoord.y))),
        smoothstep(-lVCellSmoothing.b, 0.6, abs(cos(PI * lVCellFreq.b * pNormalizedCoord.y))));
    vec3 lCellMask = lHCellMask * lVCellMask;

    // Map the normalized screen coordinate to a discrete cell coordinate in the range [-1, 1].
    vec3 lHCellCoord = vec3(
        round(pNormalizedCoord.x * lHCellFreq.r) / lHCellFreq.r,
        round(pNormalizedCoord.x * lHCellFreq.g) / lHCellFreq.g,
        round(pNormalizedCoord.x * lHCellFreq.b) / lHCellFreq.b);
    vec3 lVCellCoord = vec3(
        round(pNormalizedCoord.y * lVCellFreq.r) / lVCellFreq.r,
        round(pNormalizedCoord.y * lVCellFreq.g) / lVCellFreq.g,
        round(pNormalizedCoord.y * lVCellFreq.b) / lVCellFreq.b);

    // Sample the signal amplitude at the discrete cell coordinate.
    vec3 lSignalAmplitude = SampleSignalAmplitude(lHCellCoord);

    // Map the normalized signal amplitude to a discrete cell coordinate in the range [0, 1].
    vec3 lVSignalMaxCellCoord = vec3(
        round(abs(lSignalAmplitude.r) * lVCellFreq.r) / lVCellFreq.r,
        round(abs(lSignalAmplitude.g) * lVCellFreq.g) / lVCellFreq.g,
        round(abs(lSignalAmplitude.b) * lVCellFreq.b) / lVCellFreq.b);

    // Compute the signal amplitude mask with respect to the discrete cell coordinate.
    vec3 lSignalAmplitudeSmoothing = SampleSignalAmplitudeSmoothing(lHCellCoord);
    vec3 lSignalAmplitudeMask = vec3(
        smoothstep(-lSignalAmplitudeSmoothing.r, 0.01, lVSignalMaxCellCoord.r - abs(lVCellCoord.r)),
        smoothstep(-lSignalAmplitudeSmoothing.g, 0.01, lVSignalMaxCellCoord.g - abs(lVCellCoord.g)),
        smoothstep(-lSignalAmplitudeSmoothing.b, 0.01, lVSignalMaxCellCoord.b - abs(lVCellCoord.b)));

    // Sample the signal intensity at the discrete cell coordinate.
    vec3 lSignalIntensity = SampleSignalIntensity(lHCellCoord);

    // Combine the signal with the cell pattern.
    return lCellMask * lSignalAmplitudeMask * lSignalIntensity;
}

void main()
{
    // Compute normalized coordinates in the range [-1, 1] with the center point at (0, 0).
    vec2 lNormalizedCoord = (gl_FragCoord.xy - uFrameResolution.xy * 0.5) / (uFrameResolution.xy * 0.5);

    FragmentColor = vec4(ComputeCellIntensity(lNormalizedCoord), 1.0);
}
