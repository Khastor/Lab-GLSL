#version 440

///////////////////////////////////////////////////////////////////////////////////////////////////
// Practice > Functions                                                                          //
///////////////////////////////////////////////////////////////////////////////////////////////////

#define PI 3.14159265359

/**
 * @brief The frame time, in seconds.
 */
uniform float uFrameTime;

/**
 * @brief The viewport resolution, in pixels.
 */
uniform vec2 uFrameResolution;

/**
 * @brief The fragment shader output color.
 */
out vec4 FragmentColor;

vec4 Plot(in vec2 pPoint, in vec2 pCurve, in vec3 pCurveColor, in float pCurveWidth)
{
    float lCurveFactor = smoothstep(0.5 * pCurveWidth, pCurveWidth, abs(pPoint.y - pCurve.y));
    return vec4(lCurveFactor * vec3(pCurve.y) + (1.0 - lCurveFactor) * pCurveColor, 1.0);
}

void main()
{
    vec2 lPoint = gl_FragCoord.xy / uFrameResolution.xy;
    vec2 lCurve = vec2(lPoint.x, 0.5 + 0.1 * (ceil(1.5 * sin(2.0 * PI * lPoint.x + 3.0 * uFrameTime)) + floor(2.5 * sin(5.0 * PI * lPoint.x - uFrameTime))));

    FragmentColor = Plot(lPoint, lCurve, vec3(1.0, 0.0, 0.0), 0.04);
}
