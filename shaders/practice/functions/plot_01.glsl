#version 440

///////////////////////////////////////////////////////////////////////////////////////////////////
// Practice > Functions                                                                          //
///////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * @brief The curve color.
 *
 * @label Color
 * @widget color
 */
uniform vec4 uColor;

/**
 * @brief The viewport resolution, in pixels.
 */
uniform vec2 uFrameResolution;

/**
 * @brief The fragment shader output color.
 */
out vec4 FragmentColor;

vec4 Plot(in vec2 pPoint, in vec2 pCurve, in vec3 pCurveColor, in float pCurveWidth)
{
    float lCurveFactor = smoothstep(0.5 * pCurveWidth, pCurveWidth, abs(pPoint.y - pCurve.y));
    return vec4(lCurveFactor * vec3(pCurve.y) + (1.0 - lCurveFactor) * pCurveColor, 1.0);
}

void main()
{
    vec2 lPoint = gl_FragCoord.xy / uFrameResolution.xy;
    vec2 lCurve = vec2(lPoint.x, 0.5 + 0.5 * cos(10.0 * lPoint.x));

    FragmentColor = Plot(lPoint, lCurve, uColor.rgb, 0.04);
}
