#version 440

///////////////////////////////////////////////////////////////////////////////////////////////////
// Practice > Functions                                                                          //
///////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * @brief The frame time, in seconds.
 */
uniform float uFrameTime;

/**
 * @brief The viewport resolution, in pixels.
 */
uniform vec2 uFrameResolution;

/**
 * @brief The fragment shader output color.
 */
out vec4 FragmentColor;

vec4 Plot(in vec2 pPoint, in vec2 pCurve, in vec3 pCurveColor, in float pCurveWidth)
{
    float lCurveFactor = 1.0 - smoothstep(0.5 * pCurveWidth, pCurveWidth, abs(pPoint.y - pCurve.y));
    return vec4(lCurveFactor * pCurveColor, 1.0);
}

void main()
{
    vec2 lPoint = gl_FragCoord.xy / uFrameResolution.xy;
    float lSample = abs(fract(0.5 * lPoint.x + 0.2 * uFrameTime) - 0.5);
    vec2 lCurve1 = vec2(lPoint.x, pow(0.1, lSample));
    vec2 lCurve2 = vec2(lPoint.x, sqrt(lSample));
    vec2 lCurve3 = vec2(lPoint.x, log(1.0 + lSample));
    
    FragmentColor = vec4(0.0, 0.0, 0.0, 1.0);
    FragmentColor += Plot(lPoint, lCurve1, vec3(1.0, 0.0, 0.0), 0.04);
    FragmentColor += Plot(lPoint, lCurve2, vec3(0.0, 1.0, 0.0), 0.04);
    FragmentColor += Plot(lPoint, lCurve3, vec3(0.0, 0.0, 1.0), 0.04);
}
