#version 440

///////////////////////////////////////////////////////////////////////////////////////////////////
// Practice > Functions                                                                          //
///////////////////////////////////////////////////////////////////////////////////////////////////

#define PI 3.14159265359

/**
 * @brief The frame time, in seconds.
 */
uniform float uFrameTime;

/**
 * @brief The viewport resolution, in pixels.
 */
uniform vec2 uFrameResolution;

/**
 * @brief The fragment shader output color.
 */
out vec4 FragmentColor;

void main()
{
    vec2 lPoint = gl_FragCoord.xy / uFrameResolution.xy;
    float lNumberOfSteps = 3.0;
    float lCurrentStep = floor(lPoint.x * lNumberOfSteps);
    float lCurrentFactor = fract(lPoint.x * lNumberOfSteps);

    // Compute hard step.
    float lCurveHardValue = (lCurrentStep + step(0.5, lCurrentFactor)) / lNumberOfSteps;
    vec4 lCurveHard = vec4(vec3(lCurveHardValue), 1.0);

    // Compute smooth step.
    float lCurveSmoothValue = (lCurrentStep + smoothstep(0.2, 0.8, lCurrentFactor)) / lNumberOfSteps;
    vec4 lCurveSmooth = vec4(vec3(lCurveSmoothValue), 1.0);

    // Display an alternating mix of hard step and smooth step.
    float lCurveSmoothness = smoothstep(-0.4, 0.4, sin(uFrameTime + lPoint.x));
    float lCurveSmoothnessCurve = 1.0 - step(0.01, abs(lPoint.y - lCurveSmoothness));
    vec4 lCurveSmoothnessCurveColor = vec4(0.0, 0.0, 0.0, 1.0);

    FragmentColor = mix(mix(lCurveHard, lCurveSmooth, lCurveSmoothness), lCurveSmoothnessCurveColor, lCurveSmoothnessCurve);
}
