#version 440

///////////////////////////////////////////////////////////////////////////////////////////////////
// Practice > Functions                                                                          //
///////////////////////////////////////////////////////////////////////////////////////////////////

#define PI 3.14159265359

/**
 * @brief The frame time, in seconds.
 */
uniform float uFrameTime;

/**
 * @brief The viewport resolution, in pixels.
 */
uniform vec2 uFrameResolution;

/**
 * @brief The fragment shader output color.
 */
out vec4 FragmentColor;

vec3 ComputeSignalIntensity(in vec2 pNormalizedCoord)
{
    vec3 lHorizontalMask = vec3(
        smoothstep(-1.5, -0.3, cos(32.0 * pNormalizedCoord.x)),
        smoothstep(-1.2, -0.1, cos(31.8 * pNormalizedCoord.x)),
        smoothstep(-1.8, -0.1, cos(31.6 * pNormalizedCoord.x)));
    vec3 lVerticalMask = vec3(smoothstep(0.0, 0.8, abs(cos(64.0 * pNormalizedCoord.y))));
    return lHorizontalMask * lVerticalMask;
}

vec3 ComputeSignalAmplitude(in vec2 pNormalizedCoord)
{
    vec3 lShape = vec3(
        0.5 + 0.25 * sin(PI * -2.0 * uFrameTime + 4.0 * pNormalizedCoord.x),
        0.5 + 0.20 * sin(PI * -2.0 * uFrameTime + 0.1 + 4.2 * pNormalizedCoord.x),
        0.5 + 0.22 * sin(PI * -2.0 * uFrameTime + 0.2 + 4.5 * pNormalizedCoord.x));
    return lShape * vec3(0.2 + 0.5 * (1.0 - smoothstep(0.4, 0.8, abs(pNormalizedCoord.x - 0.2))));
}

vec3 ComputeSignalSmoothing(in vec2 pNormalizedCoord)
{
    return vec3(
        abs(pNormalizedCoord.y * 0.4 * (0.5 + 1.5 * abs(pNormalizedCoord.x))),
        abs(pNormalizedCoord.y * 0.3),
        abs(pNormalizedCoord.y * 32.0 * mod(pNormalizedCoord.x, 1.0 / 32.0)));
}

void main()
{
    // Compute normalized, centered coordinates preserving the aspect ratio.
    vec2 lNormalizedCoord = (gl_FragCoord.xy - uFrameResolution.xy * 0.5) / (vec2(min(uFrameResolution.x, uFrameResolution.y)) * 0.5);

    vec3 lSignalAmplitude = ComputeSignalAmplitude(lNormalizedCoord);
    vec3 lSignalSmoothing = ComputeSignalSmoothing(lNormalizedCoord);
    float lSignalAmplitudeR = 1.0 - smoothstep(lSignalAmplitude.r, lSignalAmplitude.r + lSignalSmoothing.r, abs(lNormalizedCoord.y));
    float lSignalAmplitudeG = 1.0 - smoothstep(lSignalAmplitude.g, lSignalAmplitude.g + lSignalSmoothing.g, abs(lNormalizedCoord.y));
    float lSignalAmplitudeB = 1.0 - smoothstep(lSignalAmplitude.b, lSignalAmplitude.b + lSignalSmoothing.b, abs(lNormalizedCoord.y));
    vec3 lSignalAmplitudeMask = vec3(lSignalAmplitudeR, lSignalAmplitudeG, lSignalAmplitudeB);

    FragmentColor = vec4(lSignalAmplitudeMask * ComputeSignalIntensity(lNormalizedCoord), 1.0);
}
