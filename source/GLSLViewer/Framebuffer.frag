#version 440

/**
 * @brief The viewport resolution, in pixels.
 */
uniform vec2 uFrameResolution;

/**
 * @brief The fragment shader output color.
 */
out vec4 FragmentColor;

void main()
{
    const ivec2 lTileCoord = ivec2(ceil((gl_FragCoord.xy - uFrameResolution.xy * 0.5) / 24.0));
    FragmentColor = vec4(mix(vec3(0.9), vec3(0.7), (lTileCoord.x + lTileCoord.y) % 2), 1.0);
}