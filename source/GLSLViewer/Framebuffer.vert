#version 440

/**
 * @brief Framebuffer vertex coordinates (top-left triangle then bottom right triangle, CCW order).
 */
const vec2 FramebufferVertexCoordinates[6] =
{
    vec2(-1.0, +1.0), vec2(-1.0, -1.0), vec2(+1.0, +1.0),
    vec2(+1.0, -1.0), vec2(+1.0, +1.0), vec2(-1.0, -1.0)
};

void main()
{
    gl_Position = vec4(FramebufferVertexCoordinates[gl_VertexID], 0.0, 1.0);
}
