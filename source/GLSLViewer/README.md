# GLSL Viewer

The GLSL Viewer is a simple tool to render GLSL shaders and edit uniform variables on the fly.

## Rendering GLSL Shaders

To render a GLSL fragment shader, drag and drop the GLSL fragment shader source file into the GLSL Viewer.

## Using Built-In Uniforms

The GLSL Viewer provides a set of built-in uniforms for use in GLSL fragment shaders.

| Type        | Name                    | Description                                                                 |
| :---------- | :---------------------- | :-------------------------------------------------------------------------- |
| `vec2`      | uFrameResolution        | The frame width and height, in pixels                                       |
| `float`     | uFrameTime              | The frame time, in seconds                                                  |
| `float`     | uFrameDeltaTime         | The time elapsed since the previous frame, in seconds                       |
| `float`     | uFrameRate              | The frame rate, in Hz                                                       |

In order to use a built-in uniform, the GLSL fragment shader source code must include its declaration.

Built-in uniforms are not displayed in the user interface but are updated every frame.

## Using Custom Uniforms

Any uniform declaration in the GLSL fragment shader source code that does not match exactly the type and name of a built-in uniform is a custom uniform.

The GLSL Viewer supports the following uniform basic types:

| Type                                  | Description                                                                 |
| :------------------------------------ | :-------------------------------------------------------------------------- |
| `float`, `vec2`, `vec3`, `vec4`       | Single-precision floating points (scalar and vectors)                       |
| `int`, `ivec2`, `ivec3`, `ivec4`      | Signed two's complement 32-bit integers (scalar and vectors)                |
| `uint`, `uvec2`, `uvec3`, `uvec4`     | Unsigned 32-bit integers (scalar and vectors)                               |
| `bool`                                | Booleans (scalar)                                                           |

Uniform arrays are not supported.

Custom uniforms are displayed in the user interface and can be updated on the fly. The display and range of valid values for a uniform can be specified with uniform annotations.

## Uniform Annotations

Uniform annotations can be defined in multi-line comment blocks directly before a uniform declaration.
A uniform annotation cannot be defined more than once whithin the same multi-line comment block, and its `@tag` must not appear in any other context than this definition.

The following block of code declares a vec2 uniform with a brief description, a display label, a default value and a range of valid values for the components of the vec2.

```glsl
/**
 * @brief The size of the rectangle.
 *
 * @label Rectangle Size
 * @default 100.0, 100.0
 * @minimum 1.0
 * @maximum 500.0
 */
uniform vec2 uRectangleSize;
```

The GSLS Viewer supports the following uniform annotations:

| Uniform Annotation Tag      | Description                                                                                       |
| :-------------------------- | :------------------------------------------------------------------------------------------------ |
| `@brief`                    | The brief description shown in a tooltip when hovering the uniform widget in the user interface   |
| `@label`                    | The display label always shown next to the uniform widget in the user interface                   |
| `@default`                  | The default value (scalar or comma-separated vector components)                                   |
| `@minimum`                  | The minimum value for all components (scalar)                                                     |
| `@maximum`                  | The maximum value for all components (scalar)                                                     |
| `@widget`                   | A hint to the user interface to show a special type of widget for this uniform type               |

However, not every uniform annotation is supported by all uniform types. Unsupported uniform annotations are ignored.

The following sections provide a detailed description of uniform annotations and the uniform types to which they can be applied.

### The brief Uniform Annotation

The `@brief` uniform annotation is supported by all uniform types.

It specifies the brief description shown in a tooltip when hovering the uniform widget in the user interface.
If this annotation is not set, the tooltip will not be shown.

The `@brief` tag must be followed by the textual description of the uniform, which stops at the end of the line. Leading whitespace characters are discarded.

### The label Uniform Annotation

The `@label` uniform annotation is supported by all uniform types.

It specifies the display label always shown next to the uniform widget in the user interface.
If this annotation is not set, the default display label is the declared uniform name in the GLSL source code.

The `@label` tag must be followed by the display label of the uniform, which stops at the end of the line. Leading whitespace characters are discarded.

### The default Uniform Annotation

The `@default` uniform annotation is supported by all uniform basic types except `bool` which always default to false.

It specifies the default value of the uniform variable.
If this annotation is not set, the default value for scalar and vector uniforms is null.
If not enough values are provided to initialize every component of a vector uniform, the remaining components are null.

The `@default` tag must be followed by either a single value for a scalar uniform or a comma-separated list of values for each component of a vector uniform.

### The minimum Uniform Annotation

The `@minimum` uniform annotation is supported by all uniform basic types except `bool`.

It specifies the minimum value for all components of the uniform.
If this annotation is not set, the minimum value for all components of the uniform is the minimum value for that uniform type.

The `@minimum` tag must be followed by a single value.

### The maximum Uniform Annotation

The `@maximum` uniform annotation is supported by all uniform basic types except `bool`.

It specifies the maximum value for all components of the uniform.
If this annotation is not set, the maximum value for all components of the uniform is the maximum value for that uniform type.

The `@maximum` tag must be followed by a single value.

### The widget Uniform Annotation

The `@widget` uniform annotation gives a hint to the user interface to show a special type of widget for that uniform type.

The `@widget` tag must be followed by a token that specifies the type of widget to show in the user interface.

The supported pairs of uniform type and widget token are listed below:

| Type        | Widget                  | Description                                                                 |
| :---------- | :---------------------- | :-------------------------------------------------------------------------- |
| `vec4`      | `color`                 | Replace the default `vec4` widget by a RGBA color picker                    |
