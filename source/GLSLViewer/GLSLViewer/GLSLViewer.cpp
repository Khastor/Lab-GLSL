///////////////////////////////////////////////////////////////////////////////////////////////////
// GLSL Viewer Implementation                                                                    //
///////////////////////////////////////////////////////////////////////////////////////////////////

// -------------------------------------------------------------------------------- Project Headers
#include <GLSLViewer/GLSLViewer.hpp>

// -------------------------------------------------------------------------------- Library Headers
#include <imgui.h>

#include <stb/stb_image_write.h>

// --------------------------------------------------------------------------------- System Headers
#include <algorithm>
#include <array>
#include <chrono>
#include <cstddef>
#include <cstdlib>
#include <cstring>
#include <format>
#include <fstream>
#include <iostream>
#include <utility>

namespace
{

constexpr const char* BUILTIN_VERTEX_SHADER_SOURCE_FILE{ "Framebuffer.vert" };
constexpr const char* BUILTIN_FRAGMENT_SHADER_SOURCE_FILE{ "Framebuffer.frag" };
constexpr std::size_t SHADER_SOURCE_BUFFER_SIZE{ 1ULL << 16 };

constexpr std::string_view GLSL_TOKEN_SEPARATORS{ " \t\r\n" };
constexpr std::string_view GLSL_TOKEN_SEPARATORS_IN_LINE{ " \t" };
constexpr std::string_view GLSL_TOKEN_SEPARATORS_NEW_LINE{ "\r\n" };
constexpr std::string_view GLSL_TOKEN_COMMENT_OPEN{ "/*" };
constexpr std::string_view GLSL_TOKEN_COMMENT_CLOSE{ "*/" };
constexpr std::string_view GLSL_TOKEN_UNIFORM{ "uniform" };

template<typename tValueT>
tValueT GetScalarOrZero(
    const GLchar* pSourceIt);

template<>
GLfloat GetScalarOrZero<GLfloat>(
    const GLchar* pSourceIt)
{
    return static_cast<GLfloat>(std::atof(pSourceIt));
}

template<>
GLint GetScalarOrZero<GLint>(
    const GLchar* pSourceIt)
{
    return static_cast<GLint>(std::atoll(pSourceIt));
}

template<>
GLuint GetScalarOrZero<GLuint>(
    const GLchar* pSourceIt)
{
    return static_cast<GLuint>(std::atoll(pSourceIt));
}

template<typename tValueT>
void ReadUniformAnnotationValues(
    std::string_view pSource,
    tValueT* pValuesOutputIt,
    GLsizei pValuesOutputCount = 1)
{
    const tValueT* const lValuesOutputEndIt{ pValuesOutputIt + pValuesOutputCount };
    while (!pSource.empty() && pValuesOutputIt != lValuesOutputEndIt)
    {
        *pValuesOutputIt++ = GetScalarOrZero<tValueT>(pSource.data());

        if (const std::size_t lNextOffset{ pSource.find(',') }; lNextOffset != std::string_view::npos)
        {
            pSource.remove_prefix(lNextOffset + 1);
        }
        else
        {
            return;
        }
    }
}

} // anonymous namespace

GLSLViewer::GLSLViewer() :
    CG::Platform::OpenGLWindow(4, 4, 1, "GLSL Viewer"),
    mMemory(1ULL << 24)
{}

bool GLSLViewer::OnCreate()
{
    CG::Core::MemoryStack::Frame lFrameAllocator{ mMemory.MakeStackFrame() };

    ///////////////////////////////////////////////////////////////////////////////////////////////
    // Read the built-in vertex shader and fragment shader sources                               //
    ///////////////////////////////////////////////////////////////////////////////////////////////

    GLchar* lBuiltInVertexShaderSourceBuffer{ lFrameAllocator.Allocate(SHADER_SOURCE_BUFFER_SIZE) };
    GLint lBuiltInVertexShaderSourceLength{};
    if (std::ifstream lBuiltInVertexShaderSourceFile{ BUILTIN_VERTEX_SHADER_SOURCE_FILE })
    {
        lBuiltInVertexShaderSourceFile.read(lBuiltInVertexShaderSourceBuffer, SHADER_SOURCE_BUFFER_SIZE);
        lBuiltInVertexShaderSourceLength = lBuiltInVertexShaderSourceFile.gcount();
        if (!lBuiltInVertexShaderSourceFile.eof())
        {
            return false;
        }
    }
    else
    {
        return false;
    }

    GLchar* lBuiltInFragmentShaderSourceBuffer{ lFrameAllocator.Allocate(SHADER_SOURCE_BUFFER_SIZE) };
    GLint lBuiltInFragmentShaderSourceLength{};
    if (std::ifstream lBuiltInFragmentShaderSourceFile{ BUILTIN_FRAGMENT_SHADER_SOURCE_FILE })
    {
        lBuiltInFragmentShaderSourceFile.read(lBuiltInFragmentShaderSourceBuffer, SHADER_SOURCE_BUFFER_SIZE);
        lBuiltInFragmentShaderSourceLength = lBuiltInFragmentShaderSourceFile.gcount();
        if (!lBuiltInFragmentShaderSourceFile.eof())
        {
            return false;
        }
    }
    else
    {
        return false;
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////
    // Compile the built-in vertex shader and fragment shader sources                            //
    ///////////////////////////////////////////////////////////////////////////////////////////////

    mBuiltInVertexShader = glCreateShader(GL_VERTEX_SHADER);
    glShaderSource(mBuiltInVertexShader, 1, &lBuiltInVertexShaderSourceBuffer, &lBuiltInVertexShaderSourceLength);
    glCompileShader(mBuiltInVertexShader);

    GLint lBuiltInVertexShaderInfoLogLength{};
    glGetShaderiv(mBuiltInVertexShader, GL_INFO_LOG_LENGTH, &lBuiltInVertexShaderInfoLogLength);

    if (lBuiltInVertexShaderInfoLogLength)
    {
        GLchar* lInfoLogBuffer{ lFrameAllocator.Allocate(lBuiltInVertexShaderInfoLogLength) };
        glGetShaderInfoLog(mBuiltInVertexShader, lBuiltInVertexShaderInfoLogLength, 0, lInfoLogBuffer);
        std::cerr << BUILTIN_VERTEX_SHADER_SOURCE_FILE << ":\n" << lInfoLogBuffer << std::endl;
    }

    GLint lBuiltInVertexShaderCompileStatus{};
    glGetShaderiv(mBuiltInVertexShader, GL_COMPILE_STATUS, &lBuiltInVertexShaderCompileStatus);
    if (lBuiltInVertexShaderCompileStatus != GL_TRUE)
    {
        return false;
    }

    const GLuint lBuiltInFragmentShader{ glCreateShader(GL_FRAGMENT_SHADER) };
    glShaderSource(lBuiltInFragmentShader, 1, &lBuiltInFragmentShaderSourceBuffer, &lBuiltInFragmentShaderSourceLength);
    glCompileShader(lBuiltInFragmentShader);

    GLint lBuiltInFragmentShaderInfoLogLength{};
    glGetShaderiv(lBuiltInFragmentShader, GL_INFO_LOG_LENGTH, &lBuiltInFragmentShaderInfoLogLength);

    if (lBuiltInFragmentShaderInfoLogLength)
    {
        GLchar* lInfoLogBuffer{ lFrameAllocator.Allocate(lBuiltInFragmentShaderInfoLogLength) };
        glGetShaderInfoLog(lBuiltInFragmentShader, lBuiltInFragmentShaderInfoLogLength, 0, lInfoLogBuffer);
        std::cerr << BUILTIN_FRAGMENT_SHADER_SOURCE_FILE << ":\n" << lInfoLogBuffer << std::endl;
    }

    GLint lBuiltInFragmentShaderCompileStatus{};
    glGetShaderiv(lBuiltInFragmentShader, GL_COMPILE_STATUS, &lBuiltInFragmentShaderCompileStatus);
    if (lBuiltInFragmentShaderCompileStatus != GL_TRUE)
    {
        return false;
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////
    // Link the built-in shader program                                                          //
    ///////////////////////////////////////////////////////////////////////////////////////////////

    mBuiltInProgram = glCreateProgram();
    glAttachShader(mBuiltInProgram, mBuiltInVertexShader);
    glAttachShader(mBuiltInProgram, lBuiltInFragmentShader);
    glLinkProgram(mBuiltInProgram);
    glDetachShader(mBuiltInProgram, mBuiltInVertexShader);
    glDetachShader(mBuiltInProgram, lBuiltInFragmentShader);
    glDeleteShader(lBuiltInFragmentShader);

    GLint lBuiltInShaderProgramInfoLogLength{};
    glGetProgramiv(mBuiltInProgram, GL_INFO_LOG_LENGTH, &lBuiltInShaderProgramInfoLogLength);

    if (lBuiltInShaderProgramInfoLogLength)
    {
        GLchar* lInfoLogBuffer{ lFrameAllocator.Allocate(lBuiltInShaderProgramInfoLogLength) };
        glGetProgramInfoLog(mBuiltInProgram, lBuiltInShaderProgramInfoLogLength, 0, lInfoLogBuffer);
        std::cerr << lInfoLogBuffer << std::endl;
    }

    GLint lBuiltInShaderProgramLinkStatus{};
    glGetProgramiv(mBuiltInProgram, GL_LINK_STATUS, &lBuiltInShaderProgramLinkStatus);
    if (lBuiltInShaderProgramLinkStatus != GL_TRUE)
    {
        return false;
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////
    // Initialize the dummy vertex array                                                         //
    ///////////////////////////////////////////////////////////////////////////////////////////////

    glGenVertexArrays(1, &mVertexArray);
    glBindVertexArray(mVertexArray);

    ///////////////////////////////////////////////////////////////////////////////////////////////
    // Initialize OpenGL state                                                                   //
    ///////////////////////////////////////////////////////////////////////////////////////////////

    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    return true;
}

bool GLSLViewer::OnUpdate(
    double pCurrentTime,
    double pElapsedTime)
{
    glClear(GL_COLOR_BUFFER_BIT);

    glUseProgram(mBuiltInProgram);
    glUniform2f(glGetUniformLocation(mBuiltInProgram, "uFrameResolution"), GetFramebufferWidth(), GetFramebufferHeight());
    glDrawArrays(GL_TRIANGLES, 0, 6);

    if (mUserProgram)
    {
        glUseProgram(mUserProgram);
        for (auto& lUniform : mUniforms)
        {
            lUniform->Update(*this, pCurrentTime, pElapsedTime);
        }
        glDrawArrays(GL_TRIANGLES, 0, 6);
    }
    return true;
}

void GLSLViewer::OnDestroy()
{
    glDeleteShader(mBuiltInVertexShader);
    glDeleteProgram(mBuiltInProgram);
    glDeleteProgram(mUserProgram);

    glDeleteVertexArrays(1, &mVertexArray);
}

void GLSLViewer::OnKeyPressed(
    CG::Platform::OpenGLWindow::KeyboardKey pKey,
    CG::Platform::OpenGLWindow::KeyboardMods pMods)
{
    switch (pKey)
    {
        case CG::Platform::OpenGLWindow::KeyboardKey::KEY_F11:
        {
            SetFullScreen(!IsFullScreen());
        }
        break;

        case CG::Platform::OpenGLWindow::KeyboardKey::KEY_S:
        {
            if (HasKeyboardMod(pMods, OpenGLWindow::KeyboardMod::MOD_CONTROL))
            {
                CG::Core::MemoryStack::Frame lFrameAllocator{ mMemory.MakeStackFrame() };

                const GLsizei lFramebufferWidth{ GetFramebufferWidth() };
                const GLsizei lFramebufferHeight{ GetFramebufferHeight() };

                if (char* lBuffer{ lFrameAllocator.Allocate(lFramebufferWidth * lFramebufferHeight * 4) })
                {
                    glReadBuffer(GL_FRONT);
                    glReadPixels(0, 0, lFramebufferWidth, lFramebufferHeight, GL_RGBA, GL_UNSIGNED_BYTE, lBuffer);

                    const std::string lFilename{ std::format("screenshot-{:%Y%m%d%H%M%S}.png", std::chrono::system_clock::now()) };
                    stbi_write_png(lFilename.c_str(), lFramebufferWidth, lFramebufferHeight, 4, lBuffer, lFramebufferWidth * 4);
                }
            }
        }
        break;
    }
}

void GLSLViewer::OnPathsDropped(
    std::uint32_t pCount,
    const char** pPaths)
{
    if (pCount && pPaths[0])
    {
        LoadShader(pPaths[0]);
    }
}

void GLSLViewer::LoadShader(
    const char* pFilename)
{
    glDeleteProgram(mUserProgram);
    mUserProgram = 0;
    mUniforms.clear();

    CG::Core::MemoryStack::Frame lFrameAllocator{ mMemory.MakeStackFrame() };

    ///////////////////////////////////////////////////////////////////////////////////////////////
    // Read the user-provided fragment shader source                                             //
    ///////////////////////////////////////////////////////////////////////////////////////////////

    GLchar* lUserFragmentShaderSourceBuffer{ lFrameAllocator.Allocate(SHADER_SOURCE_BUFFER_SIZE) };
    GLint lUserFragmentShaderSourceLength{};
    if (std::ifstream lUserFragmentShaderSourceFile{ pFilename })
    {
        lUserFragmentShaderSourceFile.read(lUserFragmentShaderSourceBuffer, SHADER_SOURCE_BUFFER_SIZE);
        lUserFragmentShaderSourceLength = lUserFragmentShaderSourceFile.gcount();
        if (!lUserFragmentShaderSourceFile.eof())
        {
            return;
        }
        else
        {
            // Ensure that the shader source buffer is a null-terminated C-style string.
            // This is not required by OpenGL, but so that C-style string functions behave well.
            if (lUserFragmentShaderSourceLength == SHADER_SOURCE_BUFFER_SIZE)
            {
                return;
            }
            lUserFragmentShaderSourceBuffer[lUserFragmentShaderSourceLength] = 0;
        }
    }
    else
    {
        return;
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////
    // Compile the user-provided fragment shader source                                          //
    ///////////////////////////////////////////////////////////////////////////////////////////////

    const GLuint lUserFragmentShader{ glCreateShader(GL_FRAGMENT_SHADER) };
    glShaderSource(lUserFragmentShader, 1, &lUserFragmentShaderSourceBuffer, &lUserFragmentShaderSourceLength);
    glCompileShader(lUserFragmentShader);

    GLint lUserFragmentShaderInfoLogLength{};
    glGetShaderiv(lUserFragmentShader, GL_INFO_LOG_LENGTH, &lUserFragmentShaderInfoLogLength);

    if (lUserFragmentShaderInfoLogLength)
    {
        GLchar* lInfoLogBuffer{ lFrameAllocator.Allocate(lUserFragmentShaderInfoLogLength) };
        glGetShaderInfoLog(lUserFragmentShader, lUserFragmentShaderInfoLogLength, 0, lInfoLogBuffer);
        std::cerr << pFilename << ":\n" << lInfoLogBuffer << std::endl;
    }

    GLint lUserFragmentShaderCompileStatus{};
    glGetShaderiv(lUserFragmentShader, GL_COMPILE_STATUS, &lUserFragmentShaderCompileStatus);
    if (lUserFragmentShaderCompileStatus != GL_TRUE)
    {
        return;
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////
    // Link the user-provided shader program                                                     //
    ///////////////////////////////////////////////////////////////////////////////////////////////

    mUserProgram = glCreateProgram();
    glAttachShader(mUserProgram, mBuiltInVertexShader);
    glAttachShader(mUserProgram, lUserFragmentShader);
    glLinkProgram(mUserProgram);
    glDetachShader(mUserProgram, mBuiltInVertexShader);
    glDetachShader(mUserProgram, lUserFragmentShader);
    glDeleteShader(lUserFragmentShader);

    GLint lUserShaderProgramInfoLogLength{};
    glGetProgramiv(mUserProgram, GL_INFO_LOG_LENGTH, &lUserShaderProgramInfoLogLength);

    if (lUserShaderProgramInfoLogLength)
    {
        GLchar* lInfoLogBuffer{ lFrameAllocator.Allocate(lUserShaderProgramInfoLogLength) };
        glGetProgramInfoLog(mUserProgram, lUserShaderProgramInfoLogLength, 0, lInfoLogBuffer);
        std::cerr << lInfoLogBuffer << std::endl;
    }

    GLint lUserShaderProgramLinkStatus{};
    glGetProgramiv(mUserProgram, GL_LINK_STATUS, &lUserShaderProgramLinkStatus);
    if (lUserShaderProgramLinkStatus != GL_TRUE)
    {
        glDeleteProgram(mUserProgram);
        mUserProgram = 0;
        return;
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////
    // Introspect the shader program uniforms                                                    //
    ///////////////////////////////////////////////////////////////////////////////////////////////

    std::vector<UniformAnnotations> lUniformAnnotations;
    UniformAnnotations lCurrentUniformAnnotations;

    // Indexed table to iterate through the current uniform annotations in a loop.
    const std::array lCurrentUniformAnnotationsIterable
    {
        &lCurrentUniformAnnotations.Brief,
        &lCurrentUniformAnnotations.Label,
        &lCurrentUniformAnnotations.Default,
        &lCurrentUniformAnnotations.Minimum,
        &lCurrentUniformAnnotations.Maximum,
        &lCurrentUniformAnnotations.Widget
    };

    std::string_view lUserFragmentShaderSourceView{ lUserFragmentShaderSourceBuffer, static_cast<std::size_t>(lUserFragmentShaderSourceLength) };

    // Search for uniform annotations within every multi-line comment block.
    // NOTE: The parsing of multi-line comments does not fully comply with the GLSL specification.
    // NOTE: The begin comment delimiter should not be recognised within a single-line comment.
    std::size_t lCommentOpenOffset;
    while ((lCommentOpenOffset = lUserFragmentShaderSourceView.find(GLSL_TOKEN_COMMENT_OPEN)) != std::string_view::npos)
    {
        const std::size_t lCommentCloseOffset{ lUserFragmentShaderSourceView.find(GLSL_TOKEN_COMMENT_CLOSE, lCommentOpenOffset) };
        if (lCommentCloseOffset == std::string_view::npos)
        {
            break;
        }

        const std::string_view lCommentScope{ lUserFragmentShaderSourceView.substr(lCommentOpenOffset, lCommentCloseOffset - lCommentOpenOffset) };
        for (auto lField : lCurrentUniformAnnotationsIterable)
        {
            const std::size_t lFieldTagOffset{ lCommentScope.find(lField->Tag) };
            if (lFieldTagOffset == std::string_view::npos)
            {
                continue;
            }

            const std::size_t lFieldTagEndOffset{ lCommentScope.find_first_of(GLSL_TOKEN_SEPARATORS, lFieldTagOffset) };
            if (lFieldTagEndOffset != lFieldTagOffset + lField->Tag.size())
            {
                continue;
            }

            const std::size_t lFieldValueOffset{ lCommentScope.find_first_not_of(GLSL_TOKEN_SEPARATORS_IN_LINE, lFieldTagEndOffset) };
            if (lFieldValueOffset == std::string_view::npos)
            {
                continue;
            }

            const std::size_t lFieldValueEndOffset{ lCommentScope.find_first_of(GLSL_TOKEN_SEPARATORS_NEW_LINE, lFieldValueOffset) };
            if (lFieldValueEndOffset == std::string_view::npos)
            {
                continue;
            }

            lField->Value = lCommentScope.substr(lFieldValueOffset, lFieldValueEndOffset - lFieldValueOffset);
        }

        lUserFragmentShaderSourceView.remove_prefix(lCommentCloseOffset + GLSL_TOKEN_COMMENT_CLOSE.size());

        const std::size_t lNextTokenOffset{ lUserFragmentShaderSourceView.find_first_not_of(GLSL_TOKEN_SEPARATORS) };
        if (lNextTokenOffset == std::string_view::npos)
        {
            break;
        }

        const std::size_t lNextTokenEndOffset{ lUserFragmentShaderSourceView.find_first_of(GLSL_TOKEN_SEPARATORS, lNextTokenOffset) };
        if (lNextTokenEndOffset == std::string_view::npos)
        {
            break;
        }

        if (lUserFragmentShaderSourceView.substr(lNextTokenOffset, lNextTokenEndOffset - lNextTokenOffset) == GLSL_TOKEN_UNIFORM)
        {
            const std::size_t lDeclarationEndOffset{ lUserFragmentShaderSourceView.find(';', lNextTokenEndOffset) };
            if (lDeclarationEndOffset == std::string_view::npos)
            {
                break;
            }

            std::string_view lDeclarationScope{ lUserFragmentShaderSourceView.substr(lNextTokenEndOffset, lDeclarationEndOffset - lNextTokenEndOffset) };

            std::size_t lNextTokenOffset;
            while ((lNextTokenOffset = lDeclarationScope.find_first_not_of(GLSL_TOKEN_SEPARATORS)) != std::string_view::npos)
            {
                const std::size_t lNextTokenEndOffset{ lDeclarationScope.find_first_of(GLSL_TOKEN_SEPARATORS, lNextTokenOffset) };
                if (lNextTokenEndOffset != std::string_view::npos)
                {
                    lCurrentUniformAnnotations.Name = lDeclarationScope.substr(lNextTokenOffset, lNextTokenEndOffset - lNextTokenOffset);
                    lDeclarationScope.remove_prefix(lNextTokenEndOffset);
                }
                else
                {
                    lCurrentUniformAnnotations.Name = lDeclarationScope.substr(lNextTokenOffset);
                    break;
                }
            }

            if (!lCurrentUniformAnnotations.Name.empty())
            {
                lUniformAnnotations.push_back(lCurrentUniformAnnotations);
            }
        }
    }

    GLint lActiveUniformsCount{};
    glGetProgramiv(mUserProgram, GL_ACTIVE_UNIFORMS, &lActiveUniformsCount);

    GLint lActiveUniformMaxLength{};
    glGetProgramiv(mUserProgram, GL_ACTIVE_UNIFORM_MAX_LENGTH, &lActiveUniformMaxLength);

    GLint lActiveUniformSize{};
    GLenum lActiveUniformType{};
    GLchar* lActiveUniformName{ lFrameAllocator.Allocate(lActiveUniformMaxLength) };
    for (GLint lActiveUniformIndex{}; lActiveUniformIndex < lActiveUniformsCount; lActiveUniformIndex++)
    {
        glGetActiveUniform(mUserProgram, lActiveUniformIndex, lActiveUniformMaxLength, 0, &lActiveUniformSize, &lActiveUniformType, lActiveUniformName);

        // Ignore uniform arrays.
        if (lActiveUniformSize != 1)
        {
            continue;
        }

        if (const GLint lActiveUniformLocation{ glGetUniformLocation(mUserProgram, lActiveUniformName) }; lActiveUniformLocation != -1)
        {
            std::unique_ptr<GLSLViewer::Uniform> lUniform{};
            if (!std::strcmp(lActiveUniformName, "uFrameResolution") && lActiveUniformType == GL_FLOAT_VEC2)
            {
                lUniform = std::make_unique<GLSLViewer::UniformBuiltInFrameResolution>();
            }
            else if (!std::strcmp(lActiveUniformName, "uFrameTime") && lActiveUniformType == GL_FLOAT)
            {
                lUniform = std::make_unique<GLSLViewer::UniformBuiltInFrameTime>();
            }
            else if (!std::strcmp(lActiveUniformName, "uFrameDeltaTime") && lActiveUniformType == GL_FLOAT)
            {
                lUniform = std::make_unique<GLSLViewer::UniformBuiltInFrameDeltaTime>();
            }
            else if (!std::strcmp(lActiveUniformName, "uFrameRate") && lActiveUniformType == GL_FLOAT)
            {
                lUniform = std::make_unique<GLSLViewer::UniformBuiltInFrameRate>();
            }
            else
            {
                const auto lActiveUniformAnnotationsIt
                {
                    std::find_if(lUniformAnnotations.cbegin(), lUniformAnnotations.cend(),
                        [lActiveUniformName](const UniformAnnotations& pUniformAnnotation)
                        {
                            return pUniformAnnotation.Name == lActiveUniformName;
                        })
                };

                switch (lActiveUniformType)
                {
                    case GL_FLOAT:
                    {
                        auto lUniformFloat{ std::make_unique<GLSLViewer::UniformFloat>() };
                        if (lActiveUniformAnnotationsIt != lUniformAnnotations.cend())
                        {
                            ReadUniformAnnotationValues(lActiveUniformAnnotationsIt->Default.Value, &lUniformFloat->Value);
                            ReadUniformAnnotationValues(lActiveUniformAnnotationsIt->Minimum.Value, &lUniformFloat->MinValue);
                            ReadUniformAnnotationValues(lActiveUniformAnnotationsIt->Maximum.Value, &lUniformFloat->MaxValue);
                        }
                        lUniform = std::move(lUniformFloat);
                    }
                    break;

                    case GL_FLOAT_VEC2:
                    {
                        auto lUniformFloatVec2{ std::make_unique<GLSLViewer::UniformFloatVec2>() };
                        if (lActiveUniformAnnotationsIt != lUniformAnnotations.cend())
                        {
                            ReadUniformAnnotationValues(lActiveUniformAnnotationsIt->Default.Value, lUniformFloatVec2->Value, 2);
                            ReadUniformAnnotationValues(lActiveUniformAnnotationsIt->Minimum.Value, &lUniformFloatVec2->MinValue);
                            ReadUniformAnnotationValues(lActiveUniformAnnotationsIt->Maximum.Value, &lUniformFloatVec2->MaxValue);
                        }
                        lUniform = std::move(lUniformFloatVec2);
                    }
                    break;

                    case GL_FLOAT_VEC3:
                    {
                        auto lUniformFloatVec3{ std::make_unique<GLSLViewer::UniformFloatVec3>() };
                        if (lActiveUniformAnnotationsIt != lUniformAnnotations.cend())
                        {
                            ReadUniformAnnotationValues(lActiveUniformAnnotationsIt->Default.Value, lUniformFloatVec3->Value, 3);
                            ReadUniformAnnotationValues(lActiveUniformAnnotationsIt->Minimum.Value, &lUniformFloatVec3->MinValue);
                            ReadUniformAnnotationValues(lActiveUniformAnnotationsIt->Maximum.Value, &lUniformFloatVec3->MaxValue);
                        }
                        lUniform = std::move(lUniformFloatVec3);
                    }
                    break;

                    case GL_FLOAT_VEC4:
                    {
                        if (lActiveUniformAnnotationsIt != lUniformAnnotations.cend())
                        {
                            if (!lActiveUniformAnnotationsIt->Widget.Value.empty())
                            {
                                if (lActiveUniformAnnotationsIt->Widget.Value == "color")
                                {
                                    auto lUniformColor{ std::make_unique<GLSLViewer::UniformColor>() };
                                    ReadUniformAnnotationValues(lActiveUniformAnnotationsIt->Default.Value, lUniformColor->Value, 4);
                                    lUniform = std::move(lUniformColor);
                                }
                            }
                            else
                            {
                                auto lUniformFloatVec4{ std::make_unique<GLSLViewer::UniformFloatVec4>() };
                                ReadUniformAnnotationValues(lActiveUniformAnnotationsIt->Default.Value, lUniformFloatVec4->Value, 4);
                                ReadUniformAnnotationValues(lActiveUniformAnnotationsIt->Minimum.Value, &lUniformFloatVec4->MinValue);
                                ReadUniformAnnotationValues(lActiveUniformAnnotationsIt->Maximum.Value, &lUniformFloatVec4->MaxValue);
                                lUniform = std::move(lUniformFloatVec4);
                            }
                        }
                        else
                        {
                            lUniform = std::make_unique<GLSLViewer::UniformFloatVec4>();
                        }
                    }
                    break;

                    case GL_INT:
                    {
                        auto lUniformInt{ std::make_unique<GLSLViewer::UniformInt>() };
                        if (lActiveUniformAnnotationsIt != lUniformAnnotations.cend())
                        {
                            ReadUniformAnnotationValues(lActiveUniformAnnotationsIt->Default.Value, &lUniformInt->Value);
                            ReadUniformAnnotationValues(lActiveUniformAnnotationsIt->Minimum.Value, &lUniformInt->MinValue);
                            ReadUniformAnnotationValues(lActiveUniformAnnotationsIt->Maximum.Value, &lUniformInt->MaxValue);
                        }
                        lUniform = std::move(lUniformInt);
                    }
                    break;

                    case GL_INT_VEC2:
                    {
                        auto lUniformIntVec2{ std::make_unique<GLSLViewer::UniformIntVec2>() };
                        if (lActiveUniformAnnotationsIt != lUniformAnnotations.cend())
                        {
                            ReadUniformAnnotationValues(lActiveUniformAnnotationsIt->Default.Value, lUniformIntVec2->Value, 2);
                            ReadUniformAnnotationValues(lActiveUniformAnnotationsIt->Minimum.Value, &lUniformIntVec2->MinValue);
                            ReadUniformAnnotationValues(lActiveUniformAnnotationsIt->Maximum.Value, &lUniformIntVec2->MaxValue);
                        }
                        lUniform = std::move(lUniformIntVec2);
                    }
                    break;

                    case GL_INT_VEC3:
                    {
                        auto lUniformIntVec3{ std::make_unique<GLSLViewer::UniformIntVec3>() };
                        if (lActiveUniformAnnotationsIt != lUniformAnnotations.cend())
                        {
                            ReadUniformAnnotationValues(lActiveUniformAnnotationsIt->Default.Value, lUniformIntVec3->Value, 3);
                            ReadUniformAnnotationValues(lActiveUniformAnnotationsIt->Minimum.Value, &lUniformIntVec3->MinValue);
                            ReadUniformAnnotationValues(lActiveUniformAnnotationsIt->Maximum.Value, &lUniformIntVec3->MaxValue);
                        }
                        lUniform = std::move(lUniformIntVec3);
                    }
                    break;

                    case GL_INT_VEC4:
                    {
                        auto lUniformIntVec4{ std::make_unique<GLSLViewer::UniformIntVec4>() };
                        if (lActiveUniformAnnotationsIt != lUniformAnnotations.cend())
                        {
                            ReadUniformAnnotationValues(lActiveUniformAnnotationsIt->Default.Value, lUniformIntVec4->Value, 4);
                            ReadUniformAnnotationValues(lActiveUniformAnnotationsIt->Minimum.Value, &lUniformIntVec4->MinValue);
                            ReadUniformAnnotationValues(lActiveUniformAnnotationsIt->Maximum.Value, &lUniformIntVec4->MaxValue);
                        }
                        lUniform = std::move(lUniformIntVec4);
                    }
                    break;

                    case GL_UNSIGNED_INT:
                    {
                        auto lUniformUInt{ std::make_unique<GLSLViewer::UniformUInt>() };
                        if (lActiveUniformAnnotationsIt != lUniformAnnotations.cend())
                        {
                            ReadUniformAnnotationValues(lActiveUniformAnnotationsIt->Default.Value, &lUniformUInt->Value);
                            ReadUniformAnnotationValues(lActiveUniformAnnotationsIt->Minimum.Value, &lUniformUInt->MinValue);
                            ReadUniformAnnotationValues(lActiveUniformAnnotationsIt->Maximum.Value, &lUniformUInt->MaxValue);
                        }
                        lUniform = std::move(lUniformUInt);
                    }
                    break;

                    case GL_UNSIGNED_INT_VEC2:
                    {
                        auto lUniformUIntVec2{ std::make_unique<GLSLViewer::UniformUIntVec2>() };
                        if (lActiveUniformAnnotationsIt != lUniformAnnotations.cend())
                        {
                            ReadUniformAnnotationValues(lActiveUniformAnnotationsIt->Default.Value, lUniformUIntVec2->Value, 2);
                            ReadUniformAnnotationValues(lActiveUniformAnnotationsIt->Minimum.Value, &lUniformUIntVec2->MinValue);
                            ReadUniformAnnotationValues(lActiveUniformAnnotationsIt->Maximum.Value, &lUniformUIntVec2->MaxValue);
                        }
                        lUniform = std::move(lUniformUIntVec2);
                    }
                    break;

                    case GL_UNSIGNED_INT_VEC3:
                    {
                        auto lUniformUIntVec3{ std::make_unique<GLSLViewer::UniformUIntVec3>() };
                        if (lActiveUniformAnnotationsIt != lUniformAnnotations.cend())
                        {
                            ReadUniformAnnotationValues(lActiveUniformAnnotationsIt->Default.Value, lUniformUIntVec3->Value, 3);
                            ReadUniformAnnotationValues(lActiveUniformAnnotationsIt->Minimum.Value, &lUniformUIntVec3->MinValue);
                            ReadUniformAnnotationValues(lActiveUniformAnnotationsIt->Maximum.Value, &lUniformUIntVec3->MaxValue);
                        }
                        lUniform = std::move(lUniformUIntVec3);
                    }
                    break;

                    case GL_UNSIGNED_INT_VEC4:
                    {
                        auto lUniformUIntVec4{ std::make_unique<GLSLViewer::UniformUIntVec4>() };
                        if (lActiveUniformAnnotationsIt != lUniformAnnotations.cend())
                        {
                            ReadUniformAnnotationValues(lActiveUniformAnnotationsIt->Default.Value, lUniformUIntVec4->Value, 4);
                            ReadUniformAnnotationValues(lActiveUniformAnnotationsIt->Minimum.Value, &lUniformUIntVec4->MinValue);
                            ReadUniformAnnotationValues(lActiveUniformAnnotationsIt->Maximum.Value, &lUniformUIntVec4->MaxValue);
                        }
                        lUniform = std::move(lUniformUIntVec4);
                    }
                    break;

                    case GL_BOOL:
                    {
                        lUniform = std::make_unique<GLSLViewer::UniformBool>();
                    }
                    break;
                }

                if (lUniform && lActiveUniformAnnotationsIt != lUniformAnnotations.cend())
                {
                    if (!lActiveUniformAnnotationsIt->Brief.Value.empty())
                    {
                        lUniform->Brief.assign(lActiveUniformAnnotationsIt->Brief.Value);
                    }

                    if (!lActiveUniformAnnotationsIt->Label.Value.empty())
                    {
                        lUniform->Label.assign(lActiveUniformAnnotationsIt->Label.Value);
                    }
                }
            }

            if (lUniform)
            {
                if (lUniform->Label.empty())
                {
                    lUniform->Label = lActiveUniformName;
                }
                lUniform->Location = lActiveUniformLocation;
                mUniforms.emplace_back(std::move(lUniform));
            }
        }
    }
}

void GLSLViewer::UniformFloat::Update(
    const GLSLViewer& pWindow,
    float pCurrentTime,
    float pElapsedTime)
{
    ImGui::DragFloat(Label.c_str(), &Value, 0.02f, MinValue, MaxValue, "%.2f", ImGuiSliderFlags_AlwaysClamp);
    if (ImGui::IsItemHovered() && !Brief.empty())
    {
        ImGui::SetTooltip("%s", Brief.c_str());
    }
    glUniform1f(Location, Value);
}

void GLSLViewer::UniformFloatVec2::Update(
    const GLSLViewer& pWindow,
    float pCurrentTime,
    float pElapsedTime)
{
    ImGui::DragFloat2(Label.c_str(), Value, 0.02f, MinValue, MaxValue, "%.2f", ImGuiSliderFlags_AlwaysClamp);
    if (ImGui::IsItemHovered() && !Brief.empty())
    {
        ImGui::SetTooltip("%s", Brief.c_str());
    }
    glUniform2fv(Location, 1, Value);
}

void GLSLViewer::UniformFloatVec3::Update(
    const GLSLViewer& pWindow,
    float pCurrentTime,
    float pElapsedTime)
{
    ImGui::DragFloat3(Label.c_str(), Value, 0.02f, MinValue, MaxValue, "%.2f", ImGuiSliderFlags_AlwaysClamp);
    if (ImGui::IsItemHovered() && !Brief.empty())
    {
        ImGui::SetTooltip("%s", Brief.c_str());
    }
    glUniform3fv(Location, 1, Value);
}

void GLSLViewer::UniformFloatVec4::Update(
    const GLSLViewer& pWindow,
    float pCurrentTime,
    float pElapsedTime)
{
    ImGui::DragFloat4(Label.c_str(), Value, 0.02f, MinValue, MaxValue, "%.2f", ImGuiSliderFlags_AlwaysClamp);
    if (ImGui::IsItemHovered() && !Brief.empty())
    {
        ImGui::SetTooltip("%s", Brief.c_str());
    }
    glUniform4fv(Location, 1, Value);
}

void GLSLViewer::UniformInt::Update(
    const GLSLViewer& pWindow,
    float pCurrentTime,
    float pElapsedTime)
{
    ImGui::DragInt(Label.c_str(), &Value, 0.1f, MinValue, MaxValue, "%d", ImGuiSliderFlags_AlwaysClamp);
    if (ImGui::IsItemHovered() && !Brief.empty())
    {
        ImGui::SetTooltip("%s", Brief.c_str());
    }
    glUniform1i(Location, Value);
}

void GLSLViewer::UniformIntVec2::Update(
    const GLSLViewer& pWindow,
    float pCurrentTime,
    float pElapsedTime)
{
    ImGui::DragInt2(Label.c_str(), Value, 0.1f, MinValue, MaxValue, "%d", ImGuiSliderFlags_AlwaysClamp);
    if (ImGui::IsItemHovered() && !Brief.empty())
    {
        ImGui::SetTooltip("%s", Brief.c_str());
    }
    glUniform2iv(Location, 1, Value);
}

void GLSLViewer::UniformIntVec3::Update(
    const GLSLViewer& pWindow,
    float pCurrentTime,
    float pElapsedTime)
{
    ImGui::DragInt3(Label.c_str(), Value, 0.1f, MinValue, MaxValue, "%d", ImGuiSliderFlags_AlwaysClamp);
    if (ImGui::IsItemHovered() && !Brief.empty())
    {
        ImGui::SetTooltip("%s", Brief.c_str());
    }
    glUniform3iv(Location, 1, Value);
}

void GLSLViewer::UniformIntVec4::Update(
    const GLSLViewer& pWindow,
    float pCurrentTime,
    float pElapsedTime)
{
    ImGui::DragInt4(Label.c_str(), Value, 0.1f, MinValue, MaxValue, "%d", ImGuiSliderFlags_AlwaysClamp);
    if (ImGui::IsItemHovered() && !Brief.empty())
    {
        ImGui::SetTooltip("%s", Brief.c_str());
    }
    glUniform4iv(Location, 1, Value);
}

void GLSLViewer::UniformUInt::Update(
    const GLSLViewer& pWindow,
    float pCurrentTime,
    float pElapsedTime)
{
    ImGui::DragScalar(Label.c_str(), ImGuiDataType_U32, &Value, 0.1f, &MinValue, &MaxValue, "%d", ImGuiSliderFlags_AlwaysClamp);
    if (ImGui::IsItemHovered() && !Brief.empty())
    {
        ImGui::SetTooltip("%s", Brief.c_str());
    }
    glUniform1ui(Location, Value);
}

void GLSLViewer::UniformUIntVec2::Update(
    const GLSLViewer& pWindow,
    float pCurrentTime,
    float pElapsedTime)
{
    ImGui::DragScalarN(Label.c_str(), ImGuiDataType_U32, Value, 2, 0.1f, &MinValue, &MaxValue, "%d", ImGuiSliderFlags_AlwaysClamp);
    if (ImGui::IsItemHovered() && !Brief.empty())
    {
        ImGui::SetTooltip("%s", Brief.c_str());
    }
    glUniform2uiv(Location, 1, Value);
}

void GLSLViewer::UniformUIntVec3::Update(
    const GLSLViewer& pWindow,
    float pCurrentTime,
    float pElapsedTime)
{
    ImGui::DragScalarN(Label.c_str(), ImGuiDataType_U32, Value, 3, 0.1f, &MinValue, &MaxValue, "%d", ImGuiSliderFlags_AlwaysClamp);
    if (ImGui::IsItemHovered() && !Brief.empty())
    {
        ImGui::SetTooltip("%s", Brief.c_str());
    }
    glUniform3uiv(Location, 1, Value);
}

void GLSLViewer::UniformUIntVec4::Update(
    const GLSLViewer& pWindow,
    float pCurrentTime,
    float pElapsedTime)
{
    ImGui::DragScalarN(Label.c_str(), ImGuiDataType_U32, Value, 4, 0.1f, &MinValue, &MaxValue, "%d", ImGuiSliderFlags_AlwaysClamp);
    if (ImGui::IsItemHovered() && !Brief.empty())
    {
        ImGui::SetTooltip("%s", Brief.c_str());
    }
    glUniform4uiv(Location, 1, Value);
}

void GLSLViewer::UniformBool::Update(
    const GLSLViewer& pWindow,
    float pCurrentTime,
    float pElapsedTime)
{
    ImGui::Checkbox(Label.c_str(), &Value);
    if (ImGui::IsItemHovered() && !Brief.empty())
    {
        ImGui::SetTooltip("%s", Brief.c_str());
    }
    glUniform1i(Location, Value);
}

void GLSLViewer::UniformColor::Update(
    const GLSLViewer& pWindow,
    float pCurrentTime,
    float pElapsedTime)
{
    ImGui::ColorEdit4(Label.c_str(), Value);
    if (ImGui::IsItemHovered() && !Brief.empty())
    {
        ImGui::SetTooltip("%s", Brief.c_str());
    }
    glUniform4fv(Location, 1, Value);
}

void GLSLViewer::UniformBuiltInFrameResolution::Update(
    const GLSLViewer& pWindow,
    float pCurrentTime,
    float pElapsedTime)
{
    glUniform2f(Location, pWindow.GetFramebufferWidth(), pWindow.GetFramebufferHeight());
}

void GLSLViewer::UniformBuiltInFrameTime::Update(
    const GLSLViewer& pWindow,
    float pCurrentTime,
    float pElapsedTime)
{
    glUniform1f(Location, pCurrentTime);
}

void GLSLViewer::UniformBuiltInFrameDeltaTime::Update(
    const GLSLViewer& pWindow,
    float pCurrentTime,
    float pElapsedTime)
{
    glUniform1f(Location, pElapsedTime);
}

void GLSLViewer::UniformBuiltInFrameRate::Update(
    const GLSLViewer& pWindow,
    float pCurrentTime,
    float pElapsedTime)
{
    glUniform1f(Location, (pElapsedTime > 0.0f) ? (1.0f / pElapsedTime) : 0.0f);
}

///////////////////////////////////////////////////////////////////////////////////////////////////
// GLSL Viewer Entry Point                                                                       //
///////////////////////////////////////////////////////////////////////////////////////////////////

int main(int argc, char **argv)
{
    GLSLViewer().Show(1280, 720);
}
