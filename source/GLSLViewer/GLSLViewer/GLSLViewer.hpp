#ifndef GLSL_VIEWER_HPP_INCLUDED
#define GLSL_VIEWER_HPP_INCLUDED

///////////////////////////////////////////////////////////////////////////////////////////////////
// GLSL Viewer Interface                                                                         //
///////////////////////////////////////////////////////////////////////////////////////////////////

// -------------------------------------------------------------------------------- Library Headers
#include <CG/Core/MemoryStack.hpp>
#include <CG/Platform/OpenGLWindow.hpp>

#include <glad/gl.h>

// --------------------------------------------------------------------------------- System Headers
#include <memory>
#include <limits>
#include <string>
#include <string_view>
#include <vector>

/**
 * GLSL Viewer window.
 */
class GLSLViewer : public CG::Platform::OpenGLWindow
{
public:
    /**
     * @brief Constructor.
     */
    GLSLViewer();

private:
    /**
     * @brief Create OpenGL resources.
     *
     * This method is called after the initialization of the OpenGL context.
     *
     * @return True if the creation of OpenGL resources was successful.
     */
    bool OnCreate() override;

    /**
     * @brief Update and render the current frame.
     *
     * @param pCurrentTime The current application time in seconds.
     * @param pElapsedTime The time since last frame in seconds.
     * @return False if an error occurred and the window should close.
     */
    bool OnUpdate(
        double pCurrentTime,
        double pElapsedTime) override;

    /**
     * @brief Destroy OpenGL resources.
     *
     * This method is called before the destruction of the OpenGL context.
     */
    void OnDestroy() override;

    /**
     * @brief Keyboard key pressed event handler.
     *
     * @param pKey The key code.
     * @param pMods The keyboard modifier codes bitmask.
     */
    void OnKeyPressed(
        CG::Platform::OpenGLWindow::KeyboardKey pKey,
        CG::Platform::OpenGLWindow::KeyboardMods pMods) override;

    /**
     * @brief File path dropped event handler.
     *
     * @param pCount The size of the array of file paths.
     * @param pPaths The array of file paths encoded in UTF-8.
     */
    void OnPathsDropped(
        std::uint32_t pCount,
        const char** pPaths) override;

    /**
     * @brief Load a fragment shader from a source file.
     *
     * @param pFilename The path to the fragment shader source file.
     */
    void LoadShader(
        const char* pFilename);

    /**
     * @brief Stack-based memory allocator for file buffers and strings.
     */
    CG::Core::MemoryStack mMemory;

    ///////////////////////////////////////////////////////////////////////////////////////////////
    // Shader Program                                                                            //
    ///////////////////////////////////////////////////////////////////////////////////////////////

    /**
     * @brief The built-in vertex shader.
     */
    GLuint mBuiltInVertexShader{ 0 };

    /**
     * @brief The built-in shader program.
     */
    GLuint mBuiltInProgram{ 0 };

    /**
     * @brief The user-supplied shader program.
     */
    GLuint mUserProgram{ 0 };

    /**
     * @brief OpenGL core profile requires binding a vertex array even for bufferless draw calls.
     */
    GLuint mVertexArray{ 0 };

    ///////////////////////////////////////////////////////////////////////////////////////////////
    // Shader Uniforms                                                                           //
    ///////////////////////////////////////////////////////////////////////////////////////////////

    /**
     * @brief GLSL source annotations that describe the behaviour of a uniform variable.
     *
     * Uniform annotations can be defined in multi-line comments directly above a uniform variable definition.
     */
    struct UniformAnnotations
    {
        struct Field
        {
            /**
             * @brief The unique tag that identifies the field.
             *
             * The unique tag that identifies the field in the GLSL source string.
             */
            const std::string_view Tag;

            /**
             * @brief The GLSL source substring that encodes the field value.
             *
             * The field value substring that follows the field tag substring within the null-terminated GLSL source string.
             * The GLSL source string must be null-terminated, but the null terminator may not be directly after the field value substring.
             */
            std::string_view Value;
        };

        Field Brief{ "@brief" };
        Field Label{ "@label" };
        Field Default{ "@default" };
        Field Minimum{ "@minimum" };
        Field Maximum{ "@maximum" };
        Field Widget{ "@widget" };

        /**
         * @brief The GLSL source substring that identifies the annotated uniform by name.
         *
         * The uniform name substring that identifies the annotated uniform within the null-terminated GLSL source string.
         * The GLSL source string must be null-terminated, but the null terminator may not be directly after the uniform name substring.
         */
        std::string_view Name;
    };

    /**
     * @brief Uniform.
     */
    struct Uniform
    {
        /**
         * @brief Destructor.
         */
        virtual ~Uniform() = default;

        /**
         * @brief Update the uniform value.
         *
         * @param pWindow The GLSL Viewer window.
         * @param pCurrentTime The current application time in seconds.
         * @param pElapsedTime The time since last frame in seconds.
         */
        virtual void Update(
            const GLSLViewer& pWindow,
            float pCurrentTime,
            float pElapsedTime) = 0;

        /**
         * @brief The uniform label.
         */
        std::string Label;

        /**
         * @brief The uniform brief description.
         */
        std::string Brief;

        /**
         * @brief The uniform location.
         */
        GLint Location{ -1 };
    };

    /**
     * @brief General-purpose float scalar uniform.
     */
    struct UniformFloat : public Uniform
    {
        /**
         * @brief Update the uniform value.
         *
         * @param pWindow The GLSL Viewer window.
         * @param pCurrentTime The current application time in seconds.
         * @param pElapsedTime The time since last frame in seconds.
         */
        void Update(
            const GLSLViewer& pWindow,
            float pCurrentTime,
            float pElapsedTime) override;

        GLfloat Value{};
        GLfloat MinValue{ std::numeric_limits<GLfloat>::lowest() };
        GLfloat MaxValue{ std::numeric_limits<GLfloat>::max() };
    };

    /**
     * @brief General-purpose float vector uniform.
     */
    struct UniformFloatVec2 : public Uniform
    {
        /**
         * @brief Update the uniform value.
         *
         * @param pWindow The GLSL Viewer window.
         * @param pCurrentTime The current application time in seconds.
         * @param pElapsedTime The time since last frame in seconds.
         */
        void Update(
            const GLSLViewer& pWindow,
            float pCurrentTime,
            float pElapsedTime) override;

        GLfloat Value[2]{};
        GLfloat MinValue{ std::numeric_limits<GLfloat>::lowest() };
        GLfloat MaxValue{ std::numeric_limits<GLfloat>::max() };
    };

    /**
     * @brief General-purpose float vector uniform.
     */
    struct UniformFloatVec3 : public Uniform
    {
        /**
         * @brief Update the uniform value.
         *
         * @param pWindow The GLSL Viewer window.
         * @param pCurrentTime The current application time in seconds.
         * @param pElapsedTime The time since last frame in seconds.
         */
        void Update(
            const GLSLViewer& pWindow,
            float pCurrentTime,
            float pElapsedTime) override;

        GLfloat Value[3]{};
        GLfloat MinValue{ std::numeric_limits<GLfloat>::lowest() };
        GLfloat MaxValue{ std::numeric_limits<GLfloat>::max() };
    };

    /**
     * @brief General-purpose float vector uniform.
     */
    struct UniformFloatVec4 : public Uniform
    {
        /**
         * @brief Update the uniform value.
         *
         * @param pWindow The GLSL Viewer window.
         * @param pCurrentTime The current application time in seconds.
         * @param pElapsedTime The time since last frame in seconds.
         */
        void Update(
            const GLSLViewer& pWindow,
            float pCurrentTime,
            float pElapsedTime) override;

        GLfloat Value[4]{};
        GLfloat MinValue{ std::numeric_limits<GLfloat>::lowest() };
        GLfloat MaxValue{ std::numeric_limits<GLfloat>::max() };
    };

    /**
     * @brief General-purpose int scalar uniform.
     */
    struct UniformInt : public Uniform
    {
        /**
         * @brief Update the uniform value.
         *
         * @param pWindow The GLSL Viewer window.
         * @param pCurrentTime The current application time in seconds.
         * @param pElapsedTime The time since last frame in seconds.
         */
        void Update(
            const GLSLViewer& pWindow,
            float pCurrentTime,
            float pElapsedTime) override;

        GLint Value{};
        GLint MinValue{ std::numeric_limits<GLint>::min() };
        GLint MaxValue{ std::numeric_limits<GLint>::max() };
    };

    /**
     * @brief General-purpose int vector uniform.
     */
    struct UniformIntVec2 : public Uniform
    {
        /**
         * @brief Update the uniform value.
         *
         * @param pWindow The GLSL Viewer window.
         * @param pCurrentTime The current application time in seconds.
         * @param pElapsedTime The time since last frame in seconds.
         */
        void Update(
            const GLSLViewer& pWindow,
            float pCurrentTime,
            float pElapsedTime) override;

        GLint Value[2]{};
        GLint MinValue{ std::numeric_limits<GLint>::min() };
        GLint MaxValue{ std::numeric_limits<GLint>::max() };
    };

    /**
     * @brief General-purpose int vector uniform.
     */
    struct UniformIntVec3 : public Uniform
    {
        /**
         * @brief Update the uniform value.
         *
         * @param pWindow The GLSL Viewer window.
         * @param pCurrentTime The current application time in seconds.
         * @param pElapsedTime The time since last frame in seconds.
         */
        void Update(
            const GLSLViewer& pWindow,
            float pCurrentTime,
            float pElapsedTime) override;

        GLint Value[3]{};
        GLint MinValue{ std::numeric_limits<GLint>::min() };
        GLint MaxValue{ std::numeric_limits<GLint>::max() };
    };

    /**
     * @brief General-purpose int vector uniform.
     */
    struct UniformIntVec4 : public Uniform
    {
        /**
         * @brief Update the uniform value.
         *
         * @param pWindow The GLSL Viewer window.
         * @param pCurrentTime The current application time in seconds.
         * @param pElapsedTime The time since last frame in seconds.
         */
        void Update(
            const GLSLViewer& pWindow,
            float pCurrentTime,
            float pElapsedTime) override;

        GLint Value[4]{};
        GLint MinValue{ std::numeric_limits<GLint>::min() };
        GLint MaxValue{ std::numeric_limits<GLint>::max() };
    };

    /**
     * @brief General-purpose unsigned int scalar uniform.
     */
    struct UniformUInt : public Uniform
    {
        /**
         * @brief Update the uniform value.
         *
         * @param pWindow The GLSL Viewer window.
         * @param pCurrentTime The current application time in seconds.
         * @param pElapsedTime The time since last frame in seconds.
         */
        void Update(
            const GLSLViewer& pWindow,
            float pCurrentTime,
            float pElapsedTime) override;

        GLuint Value{};
        GLuint MinValue{};
        GLuint MaxValue{ std::numeric_limits<GLuint>::max() };
    };

    /**
     * @brief General-purpose unsigned int vector uniform.
     */
    struct UniformUIntVec2 : public Uniform
    {
        /**
         * @brief Update the uniform value.
         *
         * @param pWindow The GLSL Viewer window.
         * @param pCurrentTime The current application time in seconds.
         * @param pElapsedTime The time since last frame in seconds.
         */
        void Update(
            const GLSLViewer& pWindow,
            float pCurrentTime,
            float pElapsedTime) override;

        GLuint Value[2]{};
        GLuint MinValue{};
        GLuint MaxValue{ std::numeric_limits<GLuint>::max() };
    };

    /**
     * @brief General-purpose unsigned int vector uniform.
     */
    struct UniformUIntVec3 : public Uniform
    {
        /**
         * @brief Update the uniform value.
         *
         * @param pWindow The GLSL Viewer window.
         * @param pCurrentTime The current application time in seconds.
         * @param pElapsedTime The time since last frame in seconds.
         */
        void Update(
            const GLSLViewer& pWindow,
            float pCurrentTime,
            float pElapsedTime) override;

        GLuint Value[3]{};
        GLuint MinValue{};
        GLuint MaxValue{ std::numeric_limits<GLuint>::max() };
    };

    /**
     * @brief General-purpose unsigned int vector uniform.
     */
    struct UniformUIntVec4 : public Uniform
    {
        /**
         * @brief Update the uniform value.
         *
         * @param pWindow The GLSL Viewer window.
         * @param pCurrentTime The current application time in seconds.
         * @param pElapsedTime The time since last frame in seconds.
         */
        void Update(
            const GLSLViewer& pWindow,
            float pCurrentTime,
            float pElapsedTime) override;

        GLuint Value[4]{};
        GLuint MinValue{};
        GLuint MaxValue{ std::numeric_limits<GLuint>::max() };
    };

    /**
     * @brief General-purpose boolean scalar uniform.
     */
    struct UniformBool : public Uniform
    {
        /**
         * @brief Update the uniform value.
         *
         * @param pWindow The GLSL Viewer window.
         * @param pCurrentTime The current application time in seconds.
         * @param pElapsedTime The time since last frame in seconds.
         */
        void Update(
            const GLSLViewer& pWindow,
            float pCurrentTime,
            float pElapsedTime) override;

        bool Value{};
    };

    /**
     * @brief Color uniform.
     */
    struct UniformColor : public Uniform
    {
        /**
         * @brief Update the uniform value.
         *
         * @param pWindow The GLSL Viewer window.
         * @param pCurrentTime The current application time in seconds.
         * @param pElapsedTime The time since last frame in seconds.
         */
        void Update(
            const GLSLViewer& pWindow,
            float pCurrentTime,
            float pElapsedTime) override;

        GLfloat Value[4]{};
    };

    /**
     * @brief Frame resolution built-in uniform.
     */
    struct UniformBuiltInFrameResolution : public Uniform
    {
        /**
         * @brief Update the uniform value.
         *
         * @param pWindow The GLSL Viewer window.
         * @param pCurrentTime The current application time in seconds.
         * @param pElapsedTime The time since last frame in seconds.
         */
        void Update(
            const GLSLViewer& pWindow,
            float pCurrentTime,
            float pElapsedTime) override;
    };

    /**
     * @brief Frame time built-in uniform.
     */
    struct UniformBuiltInFrameTime : public Uniform
    {
        /**
         * @brief Update the uniform value.
         *
         * @param pWindow The GLSL Viewer window.
         * @param pCurrentTime The current application time in seconds.
         * @param pElapsedTime The time since last frame in seconds.
         */
        void Update(
            const GLSLViewer& pWindow,
            float pCurrentTime,
            float pElapsedTime) override;
    };

    /**
     * @brief Frame delta time built-in uniform.
     */
    struct UniformBuiltInFrameDeltaTime : public Uniform
    {
        /**
         * @brief Update the uniform value.
         *
         * @param pWindow The GLSL Viewer window.
         * @param pCurrentTime The current application time in seconds.
         * @param pElapsedTime The time since last frame in seconds.
         */
        void Update(
            const GLSLViewer& pWindow,
            float pCurrentTime,
            float pElapsedTime) override;
    };

    /**
     * @brief Frame rate built-in uniform.
     */
    struct UniformBuiltInFrameRate : public Uniform
    {
        /**
         * @brief Update the uniform value.
         *
         * @param pWindow The GLSL Viewer window.
         * @param pCurrentTime The current application time in seconds.
         * @param pElapsedTime The time since last frame in seconds.
         */
        void Update(
            const GLSLViewer& pWindow,
            float pCurrentTime,
            float pElapsedTime) override;
    };

    /**
     * @brief The active uniforms in the current shader program.
     */
    std::vector<std::unique_ptr<Uniform>> mUniforms;
};

#endif // GLSL_VIEWER_HPP_INCLUDED
