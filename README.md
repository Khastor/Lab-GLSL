# Lab-CG-GLSL

Computer Graphics Lab for experimenting with GLSL fragment shading.

## Structure

### GLSL Shaders

The main part of the repository is the `shaders` directory, which contains all the GLSL shaders:

* The `library` subdirectory contains reference function implementations for dealing with shapes, noise, colors...
* The `practice` subdirectory contains my own experiments with GLSL fragment shaders.

### GLSL Viewer

The `source` directory contains the source code of the GLSL Viewer, a simple tool to render the GLSL shaders in this repository and edit uniform variables on the fly.

## Submodules

This repository depends on submodules. Don't forget to clone it with the `--recurse-submodules` flag.

* [Lab-CG-Common](https://gitlab.com/Khastor/Lab-CG-Common)

## How to build with CMake

```
$ mkdir build
$ cd build
$ cmake ..
$ cmake --build .
$ cmake --install .
```

## Resources

### Learning

* [The Book of Shaders](https://thebookofshaders.com)
* [Inigo Quilez](https://iquilezles.org/)

### Tools

* [Shadertoy](https://www.shadertoy.com)
* [Graphtoy](https://graphtoy.com/)
